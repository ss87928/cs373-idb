.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# All of these make commands must be called in root directory
all:

# auto format the code
format:
	black ./back-end/*.py

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        
	
# check the existence of check files
check: $(CFILES)

# commands for frontend
install-frontend:
	cd front-end/ && yarn

build-frontend:
	cd front-end/ && yarn build

run-frontend:
	cd front-end/ && yarn start

# TODO: find yarn equivalent?
format-frontend:
	cd front-end/ && npx prettier --write src

# TODO: find yarn equivalent?
eslint-frontend:
	cd front-end/ && npx eslint --ext js,jsx,ts,tsx --fix src
	
selenium-tests:
	python frontend/gui_tests/runSeleniumTests.py

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__
