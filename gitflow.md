## Follow this workflow to make changes to the Lowball repo.

1. Clone the repo at https://gitlab.com/lowball/cs373-idb/.
2. There are two branches to note: `main` and `development`. 
   * `main` is our production code. Any changes changes to this branch can break our website so merge requests into `main` will need to be tested and approved.
   * `development` is our development code. Merges into `development` will also require testing and approval, but we can fix any mistakes
   on this branch without breaking our production website. 
3. Every feature request and bug needs to be tracked with an issue and assigned at least one label. Before starting anything, make sure
there's an issue for it and assign it to yourself. 
4. Go to the issue in GitLab and select `Create branch` (in the dropdown menu under the `Create new merge request` button). 
5. GitLab will automatically generate a name for your branch. Keep this name. **Select `development` as your source branch!!** This is important because `development` will have the latest code that everyone has been working on. Click create branch to create it.
6. In the copy of your repo on your local machine, run `git checkout development` to switch to the `development` branch if you're not
already on it. Run `git pull` to pull the latest changes of the `development` branch and to see the branch that you just made for your issue.
7. Run `git checkout {branch-name}` to switch to the branch for your issue and begin making changes. 
8. While working on your branch, other devs may be pushing to `development`, so update your local `development` branch often to reduce the amount of merge conflicts later. Stage, commit, and push the changes to your branch then follow step 6 to update your local `development` branch. Switch back to the branch you've been working on and run `git pull origin development` to merge `development` into your branch. This may produce some merge conflicts, so resolve those conflicts and push the changes to your feature branch. 
9. When you're ready to merge your changes into `development`, go to GitLab. Find your branch and double check that the target branch is `development`. This means you plan to merge into `development` and NOT `main`. Press the `Create merge request` button to create a merge request. Assign it to yourself and assign the rest of the team as reviewers. Check the `squash commits` field. Check the `delete source branch upon merge` field. 
10. GitLab will run a pipeline on your changes to make sure they build and pass our tests (once we've implemented them). If the pipeline fails, resolve the errors and push your changes to run the pipeline again.
11. Your merge request will also tell you if your branch is behind `development`. If it is, follow step 8 and push your changes. 
12. Reviewers will be responsible for commenting on your changes to ask questions or point out mistakes. You will be responsible for resolving the comment threads they leave on your merge request. You must resolve all threads before you can merge.
13. Once your merge request is approved, you can merge into the **`development`** branch. This will create another pipeline. Make sure this pipeline succeeds. If it fails, create a new issue to address the fixes. Do this as quickly as possible because you don't want other devs to pull changes from a branch that won't build. 
14. Merges into `main` will not happen very often (most likely before every phase deadline). The only thing that should be merging into `main` is the `development` branch. The phase leader will be responsible for merging `development` into `main` before the due date.

