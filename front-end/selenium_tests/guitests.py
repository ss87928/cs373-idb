# from msilib.schema import Error
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager


class ChromeSearch(unittest.TestCase):
    def setUp(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        self.webpage = "https://development.low-ball.me"

    def test_home(self):  # 1
        driver = self.driver
        driver.get(self.webpage)
        assert driver.title == "Lowball"

    def test_players(self):  # 2
        driver = self.driver
        driver.get(self.webpage)
        expand_button = driver.find_element(
            By.XPATH, "//*[@id='root']/div/nav/div/button"
        )
        players_button = driver.find_element(
            By.XPATH, "//*[@id='basic-navbar-nav']/div/a[1]"
        )

        actions = ActionChains(driver)
        actions.click(expand_button).click(players_button).perform()
        assert driver.current_url == self.webpage + "/players"

    def test_coaches(self):  # 3
        driver = self.driver
        driver.get(self.webpage)

        expand_button = driver.find_element(
            By.XPATH, "//*[@id='root']/div/nav/div/button"
        )
        coaches_button = driver.find_element(
            By.XPATH, "//*[@id='basic-navbar-nav']/div/a[2]"
        )

        actions = ActionChains(driver)
        actions.click(expand_button).click(coaches_button).perform()
        assert driver.current_url == self.webpage + "/coaches"

    def test_teams(self):  # 4
        driver = self.driver
        driver.get(self.webpage)

        expand_button = driver.find_element(
            By.XPATH, "//*[@id='root']/div/nav/div/button"
        )
        teams_button = driver.find_element(
            By.XPATH, "//*[@id='basic-navbar-nav']/div/a[3]"
        )

        actions = ActionChains(driver)
        actions.click(expand_button).click(teams_button).perform()
        assert driver.current_url == self.webpage + "/teams"

    def test_about(self):  # 5
        driver = self.driver
        driver.get(self.webpage)

        expand_button = driver.find_element(
            By.XPATH, "//*[@id='root']/div/nav/div/button"
        )
        about_button = driver.find_element(
            By.XPATH, "//*[@id='basic-navbar-nav']/div/a[4]"
        )

        actions = ActionChains(driver)
        actions.click(expand_button).click(about_button).perform()
        assert driver.current_url == self.webpage + "/about"

    def test_coaches_nextpage(self):  # 6
        driver = self.driver
        driver.get(self.webpage + "/coaches")

        next = driver.find_element(
            By.XPATH, "//*[@id='root']/div/div/div/div[2]/button[2]"
        )
        actions = ActionChains(driver)
        actions.click(next).perform()
        assert driver.current_url == self.webpage + "/coaches"

    def test_coach_featuredplayer(self):  # 7
        driver = self.driver
        driver.get(self.webpage + "/coach/7")

        driver.implicitly_wait(300)
        driver.find_element(By.LINK_TEXT, "118").click()
        assert driver.current_url == self.webpage + "/player/118"

    def test_player_headcoach(self):  # 8
        driver = self.driver
        driver.get(self.webpage + "/player/9")

        driver.implicitly_wait(300)
        driver.find_element(By.LINK_TEXT, "16").click()
        assert driver.current_url == self.webpage + "/coach/16"

    def test_team_coach(self):  # 9
        driver = self.driver
        driver.get(self.webpage + "/team/13")

        driver.implicitly_wait(300)
        driver.find_element(By.LINK_TEXT, "13").click()
        assert driver.current_url == self.webpage + "/coach/13"

    def test_coach_team(self):  # 10
        driver = self.driver
        driver.get(self.webpage + "/coach/15")

        driver.implicitly_wait(300)
        driver.find_element(By.LINK_TEXT, "15").click()
        assert driver.current_url == self.webpage + "/team/15"

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
