import React from "react"
import { render, screen } from "@testing-library/react"
import "@testing-library/jest-dom"
import Navigation from "../src/components/Navbar"
import Splash from "../src/pages/Splash/Splash"
import About from "../src/pages/About/About"
import ModelPage from "../src/pages/ModelPage/ModelPage"
import { BASE_API_URL, MODEL_TYPE } from "../src/common/globalVars"
import { ISortOption } from "../src/components/SSF/SortDropdown"
import { IFilterGroup } from "../src/components/SSF/ModelSSFBar"
import {BrowserRouter} from 'react-router-dom'

const sortOptions: ISortOption[] = []
const filterGroups: IFilterGroup[] = []

const findNode = (dom: HTMLElement) => {
  const key = Object.keys(dom).find((key) => {
    return key.startsWith("__reactFiber$")
  })

  if (key === undefined || key.length === 0) {
    throw new Error("There is no React Node!")
  }

  return dom[key]
}

test("Correct Header Logo", async () => {
  // ARRANGE
  const pages = [
    { title: "Players", path: "/players" },
    { title: "Coaches", path: "/coaches" },
    { title: "Teams", path: "/teams" },
    { title: "About", path: "/about" },
  ]

  render(<Navigation title={"Lowball"} pages={pages} />)

  const navLogo = screen.getByAltText("tinyLogo")

  const navLogoNode = findNode(navLogo)

  expect(navLogoNode["memoizedProps"]["src"]).toBeTruthy()

})

test("Correct Header Titles", async () => {
  // ARRANGE
  const pages = [
    { title: "Players", path: "/players" },
    { title: "Coaches", path: "/coaches" },
    { title: "Teams", path: "/teams" },
    { title: "About", path: "/about" },
  ]

  render(<Navigation title={"Lowball"} pages={pages} />)

  const navTitles = screen.getAllByTestId("nav-titles")
  expect(navTitles).toHaveLength(4)

  pages.forEach((page, idx) => {
    const nav = navTitles[idx]
    expect(nav.children[0].innerHTML).toEqual(page.title)
    const reactNode = findNode(nav)
    expect(reactNode.memoizedProps.href).toEqual(page.path)
  })
})

test("Splash Page text", async () => {
  render(<Splash />)

  const welcomeText = screen.getByTestId("splash-p")

  expect(welcomeText.innerHTML).toEqual("Welcome to Lowball!")
})

test("About Page correct ppl num", async () => {
  render(<About />)

  const ppl = screen.getAllByText(/Responsibilities/)

  expect(ppl).toHaveLength(5)
})

test("test model page prev initially disabled", async () => {
  render(
    <ModelPage
      baseAPI={`${BASE_API_URL}/players`}
      headers={[
        "League",
        "Name",
        "Games Played",
        "Points Per Game",
        "Assists Per Game",
        "Rebounds Per Game",
      ]}
      type={MODEL_TYPE.PLAYER}
      sortOptions={sortOptions}
      filterGroups={filterGroups}
    />,{wrapper: BrowserRouter}
  )

  const prevButtonReactNode = findNode(screen.getByText("Previous Page"))

  expect(prevButtonReactNode.memoizedProps.disabled).toEqual(true)
})

test("counter display correctly", async () => {
  render(
    <ModelPage
      baseAPI={`${BASE_API_URL}/players`}
      headers={[
        "League",
        "Name",
        "Games Played",
        "Points Per Game",
        "Assists Per Game",
        "Rebounds Per Game",
      ]}
      type={MODEL_TYPE.PLAYER}
      sortOptions={sortOptions}
      filterGroups={filterGroups}
    />,{wrapper: BrowserRouter}
  )

  const counter = screen.getByTestId("counter")

  expect(counter.innerHTML).toMatch(/There are \d+ player on \d+ pages?/)

})

test("Lowball link is correct", async () => {
  const pages = [
    { title: "Players", path: "/players" },
    { title: "Coaches", path: "/coaches" },
    { title: "Teams", path: "/teams" },
    { title: "About", path: "/about" },
  ]

  render(<Navigation title={"Lowball"} pages={pages} />)
  
  const lowballLink = screen.getByText("Lowball")

  const lowballLinkReactNode = findNode(lowballLink);

  expect(lowballLinkReactNode["memoizedProps"]["href"]).toEqual("/")
})

test("Sport Reader link correct", async () => {
    render(<About />)

    const hyperlink = findNode(screen.getByText("Sport Rader"))

    expect(hyperlink["memoizedProps"]["href"]).toEqual("https://developer.sportradar.com/docs/read/basketball/NBA_G_League_v7")
    expect(hyperlink["memoizedProps"]["target"]).toEqual("_blank") 
    expect(hyperlink["memoizedProps"]["rel"]).toEqual("noreferrer") 
})

test("API Link correct", async () => {
  render(<About />)

  const hyperlink = findNode(screen.getByText("Lowball API Documentation"))

  expect(hyperlink["memoizedProps"]["href"]).toEqual("https://documenter.getpostman.com/view/23610428/2s83meo44X")
  expect(hyperlink["memoizedProps"]["target"]).toEqual("_blank") 
  expect(hyperlink["memoizedProps"]["rel"]).toEqual("noreferrer") 
})

test("Source Link correct", async () => {
  render(<About />)

  const hyperlink = findNode(screen.getByText("Lowball Source Code"))

  expect(hyperlink["memoizedProps"]["href"]).toEqual("https://gitlab.com/lowball/cs373-idb")
  expect(hyperlink["memoizedProps"]["target"]).toEqual("_blank") 
  expect(hyperlink["memoizedProps"]["rel"]).toEqual("noreferrer") 
})
