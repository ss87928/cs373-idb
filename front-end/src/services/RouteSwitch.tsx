import { BrowserRouter, Routes, Route } from "react-router-dom"

// Import pages
import Splash from "../pages/Splash/Splash"
import About from "../pages/About/About"
import Search from "../pages/Search/Search"
import Players from "../pages/Players/Players"
import Coaches from "../pages/Coaches/Coaches"
import Teams from "../pages/Teams/Teams"
import ErrorPage from "../pages/Error/ErrorPage"
import Navigation from "../components/Navbar"
import InstancePage from "../pages/InstancePage/InstancePage"
import VisualizationPage from "../pages/Visualizations/VisualizationPage"
import ProviderVisualizationPage from "../pages/Visualizations/ProviderVisualizations"
import { MODEL_TYPE } from "../common/globalVars"

const RouteSwitch = () => {
  return (
    <BrowserRouter>
      <Navigation
        title={"Lowball"}
        pages={[
          { title: "Search", path: "/search" },
          { title: "Players", path: "/players" },
          { title: "Coaches", path: "/coaches" },
          { title: "Teams", path: "/teams" },
          { title: "About", path: "/about" },
          { title: "Visualizations", path: "/visualizations"},
          { title: "Provider Visualizations", path: "/providervisualizations"}
        ]}
      />
      <Routes>
        <Route path="/" element={<Splash />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/search" element={<Search />}></Route>
        <Route path="/players" element={<Players />}></Route>
        <Route
          path="/player/:id"
          element={<InstancePage type={MODEL_TYPE.PLAYER} />}
        ></Route>
        <Route path="/coaches" element={<Coaches />}></Route>
        <Route
          path="/coach/:id"
          element={<InstancePage type={MODEL_TYPE.COACH} />}
        ></Route>
        <Route path="/teams" element={<Teams />}></Route>
        <Route path="/team/:id" element={<InstancePage type={MODEL_TYPE.TEAM}/>}></Route> 
        <Route path="/visualizations" element={<VisualizationPage />}></Route>
        <Route path="/providervisualizations" element={<ProviderVisualizationPage />}></Route>

        <Route path="*" element={<ErrorPage />}></Route>
      </Routes>
    </BrowserRouter>
  )
}

export default RouteSwitch
