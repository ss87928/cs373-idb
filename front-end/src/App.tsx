import React from 'react';
import './App.css';
import RouteSwitch from './services/RouteSwitch';

function App() {
  return (
    <div className="App">
      <RouteSwitch />
    </div>
  );
}

export default App;
