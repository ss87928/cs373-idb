interface IInstanceData {
  //Universal attributes
  id: string
  piclink: string
  league: string
  name: string
  twitterlink: string | null

  //Attributes in model page
  apg?: number
  gp?: number
  ppg?: number
  rpg?: number
  careergames?: number
  careerwins?: number
  careerlosses?: number
  careerratio?: number
  wins?: number
  losses?: number
  ratio?: number

  //Player specific
  //shouldn't be a string right? We can style it in frontend
  salary?: string
  teamid?: string
  gender?: string

  //team specific
  abbreviation?: string
  championships?: number
  conference?: string
  state?: string
  rank?: number
  players?: Object[]

  salary_int: number

  //coach specific
  coachid?: string
  pos?: string
  seasongames: number
  seasonlosses: number
  seasonratio: number
  seasonsoverall: number
  seasonswithfranchise: number
  seasonwins: number
  featuredplayer: Object

  coach?: Object
  team?: Object

}

export type {IInstanceData}