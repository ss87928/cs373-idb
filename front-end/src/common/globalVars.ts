let BASE_API_URL = ""
if (process.env.REACT_APP_ENV === "PROD") {
  BASE_API_URL = process.env.REACT_APP_PROD_ENDPOINT ?? ""
} else {
  BASE_API_URL = process.env.REACT_APP_DEV_ENDPOINT ?? ""
}

let BASE_URL = ""
if (process.env.REACT_APP_ENV === "PROD") {
  BASE_URL = process.env.REACT_APP_PROD_SITE ?? ""
} else {
  BASE_URL = process.env.REACT_APP_SITE ?? ""
}

enum MODEL_TYPE {
  PLAYER,
  COACH,
  TEAM,
}

function TYPE_TO_NAME(type: MODEL_TYPE, plural: boolean) {
  if (type === MODEL_TYPE.COACH) {
    return plural ? "coaches" : "coach"
  } else if (type === MODEL_TYPE.PLAYER) {
    return plural ? "players" : "player"
  } else {
    return plural ? "teams" : "team"
  }
}

const TEAM_ID_TO_NAME = {
  3: "Charlotte Hornets",
  21: "Orlando Magic",
  1: "Brooklyn Nets",
  13: "Los Angeles Lakers",
  7: "Denver Nuggets",
  2: "Boston Celtics",
  0: "Atlanta Hawks",
  20: "Oklahoma City Thunder",
  18: "New Orleans Pelicans",
  4: "Chicago Bulls",
  5: "Cleveland Cavaliers",
  6: "Dallas Mavericks",
  8: "Detroit Pistons",
  9: "Golden State Warriors",
  10: "Houston Rockets",
  11: "Indiana Pacers",
  12: "LA Clippers",
  28: "Utah Jazz",
  15: "Miami Heat",
  16: "Milwaukee Bucks",
  17: "Minnesota Timberwolves",
  19: "New York Knicks",
  22: "Philadelphia 76ers",
  23: "Phoenix Suns",
  24: "Portland Trail Blazers",
  25: "Sacramento Kings",
  26: "San Antonio Spurs",
  27: "Toronto Raptors",
  14: "Memphis Grizzlies",
  29: "Washington Wizards",
  30: "Atlanta Dream",
  31: "Chicago Sky",
  32: "Connecticut Sun",
  33: "Dallas Wings",
  34: "Indiana Fever",
  35: "Los Angeles Sparks",
  36: "Las Vegas Aces",
  37: "Minnesota Lynx",
  39: "Phoenix Mercury",
  38: "New York Liberty",
  41: "Washington Mystics",
  40: "Seattle Storm",
}

const FILTER_TO_LABEL = (filterString: string) => {
  if (filterString.startsWith("state")) {
    return "State Filter"
  } else if (filterString.startsWith("conference")) {
    return "Conference Filter"
  } else if (filterString.startsWith("league")) {
    return "League Filter"
  } else if (filterString.startsWith("pos")) {
    return "Position Filter"
  } else if (filterString.startsWith("gender")) {
    return "Gender Filter"
  } else {
    return ""
  }
}

export {
  BASE_API_URL,
  BASE_URL,
  MODEL_TYPE,
  TYPE_TO_NAME,
  TEAM_ID_TO_NAME,
  FILTER_TO_LABEL,
}
