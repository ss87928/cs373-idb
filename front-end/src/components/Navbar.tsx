import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import 'bootstrap/dist/css/bootstrap.min.css';
import tinyLogo from '../assets/tinyLogo.png';

type NavigationProps = {
  title: string;
  pages: {
    title: string,
    path: string
  }[];
}

const Navigation = ({title, pages}: NavigationProps) => {
  return (
  <Navbar bg="primary" expand="lg">    
      <Container>
	  <Navbar.Brand href="/"><img src={tinyLogo} className="tiny-logo" alt="tinyLogo" width="50" height="45"/></Navbar.Brand>
        <Navbar.Brand href="/">{title}</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            {pages.map((page, idx) => {
                return (
                  <Nav.Link data-testid="nav-titles" key={page['title'] + idx} href={page['path']}><strong>{page['title']}</strong></Nav.Link>
                )
            })}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
export default Navigation;