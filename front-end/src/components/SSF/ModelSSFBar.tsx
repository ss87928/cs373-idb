import React from "react"
import SearchBar from "./SearchBar"
import SortDropdown from "./SortDropdown"
import { ISortOption } from "./SortDropdown"
import Filter, { IFilterOption } from "./Filter"
import Stack from "@mui/material/Stack"

interface IFilterGroup {
  filterOptions: IFilterOption[]
  filterLabel: string
}

interface IModelSSFBarProps {
  handleSearchValue: React.Dispatch<React.SetStateAction<string>>
  enterDownhandler: Function | undefined
  handleSortChange: (eventKey: any, event: Object) => any
  sortOptions: ISortOption[]
  filterGroup: IFilterGroup[]
  handleFilterChange: Function
}

const ModelSSFBar = ({
  handleSearchValue,
  enterDownhandler,
  handleSortChange,
  sortOptions,
  filterGroup,
  handleFilterChange,
}: IModelSSFBarProps) => {
  return (
    <Stack alignItems="center" margin="2em">
      <Stack direction="row" width="75%" justifyContent="space-between">
        <SearchBar
          handleSearchValue={handleSearchValue}
          enterDownhandler={enterDownhandler}
        />
        <SortDropdown
          handleSortChange={handleSortChange}
          sortOptions={sortOptions}
        />
      </Stack>
      <Stack direction="row" width="75%" justifyContent="space-between" flexGrow={1}>
        {filterGroup.map(({ filterOptions, filterLabel }) => {
          return (
            <Filter
              options={filterOptions}
              changeHandler={handleFilterChange}
              filterLabel={filterLabel}
            />
          )
        })}
      </Stack>
    </Stack>
  )
}

export default ModelSSFBar
export type { IFilterGroup }
