import React from "react"
import TextField from "@mui/material/TextField"

interface ISearchBarProps {
  handleSearchValue: React.Dispatch<React.SetStateAction<string>> | undefined
  enterDownhandler: Function | undefined
}

const SearchBar = ({ handleSearchValue, enterDownhandler }: ISearchBarProps) => {
  return (
      <TextField
        sx={{flexGrow: 1}}
        type="text"
        label="Search"
        onChange={(event) => {if (handleSearchValue !== undefined) handleSearchValue(event.target.value.trim().replaceAll(/\s+/g, '+'))}}
        onKeyDown={(event) => {
          if (event.key === "Enter" && enterDownhandler !== undefined) enterDownhandler()
        }}
      ></TextField>
    // </Box>
  )
}

export default SearchBar
