import Highlighter from "react-highlight-words"

interface IHTextProps {
  searchWords: string[]
  children: string
}

const HText: React.FC<IHTextProps> = ({searchWords, children}: IHTextProps) => {
  return (
    <Highlighter
      searchWords={searchWords}
      autoEscape={true}
      textToHighlight={children}
    />
  )
}

export default HText;