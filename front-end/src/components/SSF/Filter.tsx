import React, { useState } from "react"
import Autocomplete from "@mui/material/Autocomplete"
import TextField from "@mui/material/TextField"

interface IFilterOption {
  label: string
  value: string
}

interface IFilterProps {
  options: IFilterOption[]
  filterLabel: string,
  changeHandler: Function
}

//reference: https://gitlab.com/coleweinman/swe-college-project/-/blob/main/frontend/src/components/toolbar/ChipFilterField.tsx

const Filter = ({ options, filterLabel, changeHandler }: IFilterProps) => {
  const [open, setOpen] = useState(false)

  return (
    <Autocomplete
      sx={{flexGrow: 1}}
      open={open}
      multiple
      onOpen={() => {
        if (options.length === 0) {
          setOpen(false)
        } else {
          setOpen(true)
        }
      }}
      onClose={(event, reason) => {
        return ["escape", "blur"].includes(reason) ? setOpen(false) : null
      }}
      options={options}
      onChange={(event, value) => {
        changeHandler(filterLabel, value.map(v => v.value))
      }}
      renderInput={(params) => {
        return (
          <TextField
            {...params}
            variant="outlined"
            label={filterLabel}
            placeholder={filterLabel}
            InputProps={{
              ...params.InputProps,
              style: { borderRadius: "8px" },
            }}
          />
        );
      }}

    />
  )
}

export default Filter
export type { IFilterOption }
