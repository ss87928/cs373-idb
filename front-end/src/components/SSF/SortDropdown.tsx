import React, {useState} from "react"
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'


interface ISortOption {
  label: string
  value: string
}

interface ISortDropdownProps {
  handleSortChange: Function
  sortOptions: ISortOption[]
}
const SortDropdown = ({
  handleSortChange,
  sortOptions,
}: ISortDropdownProps) => {

  const [open, setOpen] = useState(false)

  return (
    <Autocomplete
      sx={{flexGrow: 1}}
      open={open}
      onOpen={() => {
        if (sortOptions.length === 0) {
          setOpen(false)
        } else {
          setOpen(true)
        }
      }}
      onClose={(event, reason) => {
        return ["escape", "blur"].includes(reason) ? setOpen(false) : null
      }}
      options={sortOptions}
      onChange={(event, value) => {
        const selected = value?.value ?? ""
        console.log(selected)
        handleSortChange(selected)
      }}
      renderInput={(params) => {
        return (
          <TextField
            {...params}
            variant="outlined"
            label="Sort"
            placeholder="Sort"
            InputProps={{
              ...params.InputProps,
              style: { borderRadius: "8px" },
            }}
          />
        );
      }}

    />
  )
}

export default SortDropdown
export type { ISortOption }
