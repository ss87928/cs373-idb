import React from "react";
//source: https://gitlab.com/huanyu_bing/cs373-idb/-/blob/main/frontend/src/Components/Map.tsx
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';

interface IPlace {
    lat: number;
    lng: number;
}

const MapContainer = (coords : IPlace) => {
  
  const mapStyles = {        
    height: "60vh",
    width: "80%"};
  
  const defaultCenter = {
    lat: coords.lat, lng: coords.lng
  }
  
  return (
     <LoadScript
       googleMapsApiKey='AIzaSyBegjziBoTD-x092eWcx0B1Uzdjt_2z_pA'>
        <GoogleMap
          mapContainerStyle={mapStyles}
          zoom={13}
          center={defaultCenter}
        >
            <Marker position={defaultCenter} />
        </GoogleMap>
     </LoadScript>
  )
}

export default MapContainer;
export type {IPlace}
