import React from 'react'

interface IInstanceCounterProps {
  count: number,
  modelName: string
}

const InstanceCounter = ({count, modelName}: IInstanceCounterProps) => {
  const numPages = Math.ceil(count / 10)
  return (
    <div data-testid="counter">
      There are {count} {modelName} on {numPages} {numPages === 1 ? "page" : "pages"}
    </div>
  )
}

export default InstanceCounter