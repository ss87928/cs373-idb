import React, { useEffect, useState } from "react"
import axios from "axios"
import { Link } from "react-router-dom"
import Button from "react-bootstrap/Button"
import {
  MODEL_TYPE,
  TYPE_TO_NAME,
  TEAM_ID_TO_NAME,
  FILTER_TO_LABEL
} from "../../common/globalVars"
import { IInstanceData } from "../../common/types"
import InstanceCounter from "../../components/InstanceCounter"
import { ISortOption } from "../../components/SSF/SortDropdown"
import ModelSSFBar, { IFilterGroup } from "../../components/SSF/ModelSSFBar"
import HText from "../../components/SSF/HText"
import { useSearchParams } from "react-router-dom"

interface IModelPageParams {
  baseAPI: string
  headers: string[]
  type: MODEL_TYPE
  sortOptions: ISortOption[]
  filterGroups: IFilterGroup[]
}


const ModelPage = ({
  baseAPI,
  headers,
  type,
  sortOptions,
  filterGroups,
}: IModelPageParams) => {
  // state to store which page we are currently browsing
  const [page, setPage] = useState(1)
  const [instances, setInstances] = useState<IInstanceData[]>([])
  const [totalInstances, setTotalInstances] = useState(0)
  const [sortString, setSortString] = useState("")
  const [searchString, setSearchString] = useState("")
  const [filters, setFilters] = useState({})

  const [queryParams, setQueryParams] = useSearchParams()

  const handleSortChange = (eventValue: any, event: Object) => {
    setSortString(eventValue)
  }

  useEffect(() => {

    const temp = {}
    filterGroups.forEach(({ filterLabel }) => (temp[filterLabel] = []))
    setFilters(temp)

    if (queryParams.get("page") !== null) {
      setPage(Number(queryParams.get("page")))
    }
    if (queryParams.get("sort") !== null) {
      setSortString(queryParams.get("sort") ?? "")
    }
    if (queryParams.get("filter") !== null) {
      console.log(queryParams.get("filter"))
      setFilters(filterBuilder(queryParams.get("filter") ?? ""))
    }
    if (queryParams.get("search") !== null) {
      setSearchString(queryParams.get("search") ?? "")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {

    const queryObj = Object.fromEntries(queryParams.entries())

    //@ts-ignore
    const allFilters = [].concat(...Object.values(filters))
    const allFiltersString = allFilters.join(",")

    const newSearchParams = {...queryObj}

    newSearchParams["page"] = String(page)

    if (searchString.length === 0) {
      delete newSearchParams.search
    } else {
      newSearchParams["search"] = searchString
    }

    if (sortString.length === 0) {
      delete newSearchParams.sort
    } else {
      newSearchParams["sort"] = sortString
    }

    if (allFiltersString.length === 0) {
      delete newSearchParams.filter
    } else {
      newSearchParams["filter"] = allFiltersString
    }

    setQueryParams(newSearchParams)
    

    let url = baseAPI + "?"
    url += `page=${page}`


    if (searchString.length > 0) {
      
      url += "&"
      url += `search=${searchString}`
    }

    if (sortString.length > 0) {
      
      url += "&"
      url += `sort=${sortString}`
    }

    if (allFilters.length > 0) {
      
      url += "&"
      url += `filter=${allFiltersString}`
    }


    axios.get(url).then((res) => {
      console.log(res.data.data)
      setInstances(res.data.data)
      setTotalInstances(res.data.meta_count)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, sortString, searchString, filters])

  const filterBuilder = (filtersString: string) => {
    const result = {}
    filterGroups.forEach(({ filterLabel }) => (result[filterLabel] = [])) 
    console.log(result)
    const filters = filtersString.split(",")
    for (let filter of filters) {
      if (FILTER_TO_LABEL(filter) !== "") result[FILTER_TO_LABEL(filter)].push(filter)
    }
    return result
  }

  return (
    <div className="shadow-4 rounded-1 overflow-hidden p-1">
      <InstanceCounter
        count={totalInstances}
        modelName={TYPE_TO_NAME(type, totalInstances > 1)}
      />
      <ModelSSFBar
        handleSearchValue={setSearchString}
        enterDownhandler={undefined}
        handleSortChange={handleSortChange}
        sortOptions={sortOptions}
        filterGroup={filterGroups}
        handleFilterChange={(lbl: string, arr: string[]) => {
          const newFilers = { ...filters }
          newFilers[lbl] = arr
          setFilters(newFilers)
        }}
      />
      <table
        className="table align-middle mb-0 bg-white sortable table-hover"
        width={"100%"}
      >
        <thead style={{ color: "white", background: "#70163C" }}>
          <tr>
            {headers.map((title, idx) => {
              return (
                <th key={title + idx} style={{ cursor: "pointer" }}>
                  {title}
                </th>
              )
            })}
          </tr>
        </thead>
        <tbody>
          {instances.map((instance) => {
            return (
              <tr className="item">
                {/* League */}
                <td>
                  <HText searchWords={searchString.split(",")}>
                    {instance["league"]}
                  </HText>
                </td>

                {/* Player: id, name, img_url */}
                <td>
                  <Link
                    to={
                      `/${TYPE_TO_NAME(type, false)}/` +
                      (type === MODEL_TYPE.COACH
                        ? instance["coachid"]
                        : instance["id"])
                    }
                    className="list-group-item list-group-item-action"
                  >
                    <div className="d-flex align-items-center">
                      <img
                        src={instance["piclink"]}
                        alt=""
                        style={{ width: "45px", height: "45px" }}
                        className="rounded-circle"
                      ></img>

                      <div className="ms-3">
                        <h6 className="mb-1 mt-1">
                          <HText searchWords={searchString.split(",")}>
                            {instance["name"]}
                          </HText>
                        </h6>
                      </div>
                    </div>
                  </Link>
                </td>

                {/* Stats: gp, ppg, apg, rpg */}
                {type === MODEL_TYPE.PLAYER && (
                  <>
                    <td>{instance["gp"]}</td>
                    <td>{instance["ppg"]}</td>
                    <td>{instance["apg"]}</td>
                    <td>{instance["rpg"]}</td>
                    <td>{instance["salary"]}</td>
                    <td>
                      <HText searchWords={searchString.split(",")}>
                        {instance["pos"] ?? ""}
                      </HText>
                    </td>
                    <td>
                      <HText searchWords={searchString.split(",")}>
                        {TEAM_ID_TO_NAME[instance["teamid"] ?? -1]}
                      </HText>
                    </td>
                  </>
                )}
                {type === MODEL_TYPE.COACH && (
                  <>
                    <td>{instance["careergames"]}</td>
                    <td>{instance["careerwins"]}</td>
                    <td>{instance["careerlosses"]}</td>
                    <td>{instance["careerratio"]}</td>
                  </>
                )}
                {type === MODEL_TYPE.TEAM && (
                  <>
                    <td>
                      <HText searchWords={searchString.split(",")}>
                        {instance["abbreviation"] ?? ""}
                      </HText>
                    </td>
                    <td>{instance["gp"]}</td>
                    <td>{instance["wins"]}</td>
                    <td>{instance["losses"]}</td>
                    <td>{instance["ratio"]}</td>
                    <td>{instance["rank"]}</td>
                    <td>{instance["championships"]}</td>
                    <td>
                      <HText searchWords={searchString.split(",")}>
                        {instance["conference"] ?? ""}
                      </HText>
                    </td>
                    <td>
                      <HText searchWords={searchString.split(",")}>
                        {instance["state"] ?? ""}
                      </HText>
                    </td>
                  </>
                )}
              </tr>
            )
          })}
        </tbody>
      </table>
      <div className="d-flex justify-content-around mt-3">
        <Button
          disabled={page <= 1}
          onClick={() => {
            setPage(page - 1)
          }}
        >
          Previous Page
        </Button>
        <Button
          disabled={page >= Math.ceil(totalInstances / 10)}
          onClick={() => {
            setPage(page + 1)
          }}
        >
          Next Page
        </Button>
      </div>
    </div>
  )
}

export default ModelPage
