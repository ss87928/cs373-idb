// Code from https://dev.to/bravemaster619/simplest-way-to-embed-a-youtube-video-in-your-react-app-3bk2
import React from "react";
import PropTypes from "prop-types";

const YOUTUBE_API_BASE_URL = "https://youtube.googleapis.com/youtube/v3/"
const YOUTUBE_API_KEY = "AIzaSyCmCd-8oBtP7vhsvQblnZP5LIj7vgXRnmM"

const YoutubeEmbed = ({ embedId }) => (
  <div className="video-responsive">
    <iframe
      width="853"
      height="480"
      src={`https://www.youtube.com/embed/${embedId}`}
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      title="Embedded youtube"
    />
  </div>
);

YoutubeEmbed.propTypes = {
  embedId: PropTypes.string.isRequired
};

export default YoutubeEmbed;
export { YOUTUBE_API_BASE_URL, YOUTUBE_API_KEY };
