import axios from "axios"
import React, { useEffect, useState } from "react"
import { useLocation } from "react-router-dom"
import { MODEL_TYPE, BASE_API_URL } from "../../common/globalVars"
import { IInstanceData } from "../../common/types"
import Table from "react-bootstrap/Table"
import "./InstancePage.css"
import { Link } from "react-router-dom"
import YoutubeEmbed, { YOUTUBE_API_BASE_URL, YOUTUBE_API_KEY } from "./YoutubeEmbed";

interface IInstanceParams {
  type: MODEL_TYPE
}

function generateFeaturedPlayerLink(player_data: Object | undefined) {
  if(player_data !== undefined) {
    var parsed = JSON.parse(JSON.stringify(player_data))

    return (
      <Link to={"/player/" + parsed.id}>{parsed.name}</Link>
    );
  }
}

function generateCoachLink(coach_data: Object | undefined) {
  if(coach_data !== undefined) {
    var parsed = JSON.parse(JSON.stringify(coach_data))

    return (
      <Link to={"/coach/" + parsed.coachid}>{parsed.name}</Link>
    );
  }
}

function generateTeamLink(team_data: Object | undefined) {
  if(team_data !== undefined) {
    var parsed = JSON.parse(JSON.stringify(team_data))

    return (
      <Link to={"/team/" + parsed.id}>{parsed.name}</Link>
    );
  }
}

function generatePlayersList(players_test: Object[] | undefined) {
  if(players_test !== undefined) {
    const list : JSX.Element[] = [];
    for(var i = 0; i < players_test.length; i++) {
      var parsed = JSON.parse(JSON.stringify(players_test[i]))
      list.push(
        <li>
          <Link to={"/player/" + parsed.id}>{parsed.name}</Link>
        </li>
      )
    }

    return list;
  }
}

const InstancePage = ({ type }: IInstanceParams) => {
  const loc = useLocation()
  const [youtubeId, setYoutubeId] = useState("")
  const [info, setInfo] = useState<IInstanceData | null>(null)

  useEffect(() => {
    axios
      .get(`${BASE_API_URL}${loc.pathname}`)
      .then((data) => {
        setInfo(data.data)
        return data;
      })
      .then((data) =>
        data.data &&
        axios.get(`${YOUTUBE_API_BASE_URL}search?part=id&q=${data.data.name} basketball&key=${YOUTUBE_API_KEY}`)
          .then((data) => setYoutubeId(data.data.items[0].id.videoId))
      )

  }, [loc.pathname])

  console.log(youtubeId)

  return (
    <div>
      {/* <pre id='json'>
        {JSON.stringify(info, null, 2)}
      </pre> */}
      {type === MODEL_TYPE.PLAYER && <PlayerInstance info={info} />}
      {type === MODEL_TYPE.COACH && <CoachInstance info={info} />}
      {type === MODEL_TYPE.TEAM && <TeamInstance info={info} />}
      <YoutubeEmbed embedId={youtubeId} />
    </div>
  )
}

interface IIndividualParams {
  info: IInstanceData | null
}

const PlayerInstance = ({ info }: IIndividualParams) => {
  var coachLink = generateCoachLink(info?.coach)
  var teamLink = generateTeamLink(info?.team)
  return (
    <div className="Instance">
      <h2>{info?.name}</h2>
      <img
        style={{ marginBottom: "2%" }}
        src={info?.piclink}
        alt={`${info?.name}`}
        width="40%"
      />
      <Table striped bordered hover size="sm">
        <tbody>
          <tr>
            <td>League</td>
            <td>{info?.league}</td>
          </tr>
          <tr>
            <td>Gender</td>
            <td>{info?.gender}</td>
          </tr>
          <tr>
            <td>Position</td>
            <td>{info?.pos}</td>
          </tr>
          <tr>
            <td>Games Played</td>
            <td>{info?.gp}</td>
          </tr>
          <tr>
            <td>Points Per Game</td>
            <td>{info?.ppg}</td>
          </tr>
          <tr>
            <td>Assists Per Game</td>
            <td>{info?.apg}</td>
          </tr>
          <tr>
            <td>Rebounds Per Game</td>
            <td>{info?.rpg}</td>
          </tr>
          <tr>
            <td>Salary</td>
            <td>{info?.salary}</td>
          </tr>
          {<tr>
            <td>Head Coach</td>
            <td>
              {coachLink}
            </td>
          </tr>}
          <tr>
            <td>Team</td>
            <td>
              {teamLink}
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  )
}

const CoachInstance = ({ info }: IIndividualParams) => {
  var teamLink = generateTeamLink(info?.team)
  var featuredPlayerLink = generateFeaturedPlayerLink(info?.featuredplayer)
  return (
    <div className="Instance">
      <h2>{info?.name}</h2>
      <img
        style={{ marginBottom: "2%" }}
        src={info?.piclink}
        alt={`${info?.name}`}
        width="40%"
      />
      <Table striped bordered hover size="sm">
        <tbody>
          <tr>
            <td>League</td>
            <td>{info?.league}</td>
          </tr>
          <tr>
            <td>Position</td>
            <td>{info?.pos}</td>
          </tr>
          <tr>
            <td>Career Games</td>
            <td>{info?.careergames}</td>
          </tr>
          <tr>
            <td>Career Wins</td>
            <td>{info?.careerwins}</td>
          </tr>
          <tr>
            <td>Career Losses</td>
            <td>{info?.careerlosses}</td>
          </tr>
          <tr>
            <td>Career Ratio</td>
            <td>{info?.careerratio}</td>
          </tr>
          <tr>
            <td>Season Games</td>
            <td>{info?.seasongames}</td>
          </tr>
          <tr>
            <td>Season Wins</td>
            <td>{info?.seasonwins}</td>
          </tr>
          <tr>
            <td>Season Losses</td>
            <td>{info?.seasonlosses}</td>
          </tr>
          <tr>
            <td>Season Ratio</td>
            <td>{info?.seasonratio}</td>
          </tr>
          <tr>
            <td>Seasons Overall</td>
            <td>{info?.seasonsoverall}</td>
          </tr>
          <tr>
            <td>Seasons With Franchise</td>
            <td>{info?.seasonswithfranchise}</td>
          </tr>
          <tr>
            <td>Team</td>
            <td>
              {teamLink}
            </td>
          </tr>
          {<tr>
            <td>Featured Player</td>
            <td>
              {featuredPlayerLink}
            </td>
          </tr>}
        </tbody>
      </Table>
    </div>
  )
}

const TeamInstance = ({ info }: IIndividualParams) => {
  var playerList = generatePlayersList(info?.players)
  var coachLink = generateCoachLink(info?.coach)
  return (
    <div className="Instance">
      <h2>
        {info?.name} ({info?.abbreviation})
      </h2>
      <img
        style={{ marginBottom: "2%" }}
        src={info?.piclink}
        alt={`${info?.name}`}
        width="40%"
      />
      <Table striped bordered hover size="sm">
        <tbody>
          <tr>
            <td>League</td>
            <td>{info?.league}</td>
          </tr>
          <tr>
            <td>Rank</td>
            <td>{info?.rank}</td>
          </tr>
          <tr>
            <td>Conference</td>
            <td>{info?.conference}</td>
          </tr>
          <tr>
            <td>Championships</td>
            <td>{info?.championships}</td>
          </tr>
          <tr>
            <td>State</td>
            <td>{info?.state}</td>
          </tr>
          <tr>
            <td>Games Played</td>
            <td>{info?.gp}</td>
          </tr>
          <tr>
            <td>Games Won</td>
            <td>{info?.wins}</td>
          </tr>
          <tr>
            <td>Games Lost</td>
            <td>{info?.losses}</td>
          </tr>
          <tr>
            <td>Win/Loss Ratio</td>
            <td>{info?.ratio}</td>
          </tr>
          {<tr>
            <td>Coach</td>
            <td>
            {coachLink}
            </td>
          </tr>}
          {<tr>
              <td>Players</td>
              <td>
                {playerList}
              </td>
			    </tr>}
        </tbody>
      </Table>
    </div>
  )
}

export default InstancePage
