import logo from '../../assets/splash_photo.png';
import './Splash.css';

function Splash() {
  return (
    <div className="Splash">
      <header className="Splash-header">
        <img src={logo} className="Splash-logo" alt="logo" />
        <p data-testid="splash-p">
          Welcome to Lowball!
        </p>
      </header>
    </div>
  );
}

export default Splash;
