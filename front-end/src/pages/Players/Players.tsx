import React from 'react';
import './Players.css'
import ModelPage from '../ModelPage/ModelPage';
import { BASE_API_URL, MODEL_TYPE } from '../../common/globalVars';
import { playerFilterGroups, playerSortOptions } from './PlayerOptions';


const Players = () =>{
  // TODO: get this data from an API

  return (
    <div className="Players">
            <ModelPage 
              baseAPI={`${BASE_API_URL }/players`} 
              headers={['League', 'Name', 'Games Played', 'Points Per Game', 'Assists Per Game', 'Rebounds Per Game', 'Salary', 'Position', 'Team']}
              type={MODEL_TYPE.PLAYER}  
              sortOptions={playerSortOptions}
              filterGroups={playerFilterGroups}
            />
    </div>
  );
}
export default Players;