interface IBasePlayer {
  id: number,
  img_url: string,
  name: string,
  league: string,
  gp: number,
  ppg: number,
  apg: number,
  rpg: number
}

export type {IBasePlayer}