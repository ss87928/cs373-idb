import { ISortOption } from '../../components/SSF/SortDropdown';
import { IFilterGroup } from '../../components/SSF/ModelSSFBar';

const playerSortOptions:  ISortOption[] = [
  {
    value: "name",
    label: "⬆️ Name"
  },
  {
    value: "@name",
    label: "⬇️ Name"
  },
  {
    value: "gp",
    label: "⬆️ Games Played"
  },
  {
    value: "@gp",
    label: "⬇️ Games Played"
  },
  {
    value: "ppg",
    label: "⬆️ Points Per Game"
  },
  {
    value: "@ppg",
    label: "⬇️ Points Per Game"
  },
  {
    value: "apg",
    label: "⬆️ Assists Per Game"
  },
  {
    value: "@apg",
    label: "⬇️ Assists Per Game"
  },
  {
    value: "rpg",
    label: "⬆️ Rebounds Per Game"
  },
  {
    value: "@rpg",
    label: "⬇️ Rebounds Per Game"
  },
  {
    value: "salary_int",
    label: "⬆️ Salary"
  },
  {
    value: "@salary_int",
    label: "⬇️ Salary"
  },
]

const playerFilterGroups: IFilterGroup[] = [
  {
    filterOptions: [
      {
        label: "NBA",
        value: "leagueNBA"
      },
      {
        label: "WNBA",
        value: "leagueWNBA"
      },
    ],
    filterLabel: "League Filter"
  },
  {
    filterOptions: [
      {
        label: "Male",
        value: "genderM"
      },
      {
        label: "Female",
        value: "genderF"
      },
    ],
    filterLabel: "Gender Filter" 
  },
  {
    filterOptions: [
      {
        label: "Point Guard",
        value: "posPG"
      },
      {
        label: "Shooting Guard",
        value: "posSG"
      },
      {
        label: "Small Forward",
        value: "posSF"
      },
      {
        label: "Power Forward",
        value: "posPF"
      },
      {
        label: "Center",
        value: "posC"
      },
    ],
    filterLabel: "Position Filter" 
  },
]


export  {playerSortOptions, playerFilterGroups}