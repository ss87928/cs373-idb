import React from 'react'
import { Paper, Button, Typography } from '@mui/material'

interface ISearchMoreButton {
  count: number
  link: string
}

// code used from https://gitlab.com/coleweinman/swe-college-project/-/blob/main/frontend/src/components/search/ViewAllBar.tsx

const SearchMoreButton: React.FC<ISearchMoreButton> = ({count, link}) => {

  return (
    <Paper
      className="card"
      sx={{
        padding: "16px",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "8px",
        marginBottom: "8px",
        width: "fit-content",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        gap: "8px",
      }}
    >
      <Typography>{`${count} total results.`}</Typography>
      <Button variant="outlined" href={link}>
        View All
      </Button>
    </Paper>

  )
}

export default SearchMoreButton