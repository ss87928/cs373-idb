import axios from "axios"
import React, { useEffect, useState } from "react"
import { useSearchParams } from "react-router-dom"
import SearchBar from "../../components/SSF/SearchBar"
import { BASE_API_URL, BASE_URL, MODEL_TYPE } from "../../common/globalVars"
import { IInstanceData } from "../../common/types"
import SearchCard from "./SearchCard"
import Box from "@mui/material/Box"
import Stack from "@mui/material/Stack"
import SearchMoreButton from "./SearchMoreButton"
import Filter from "../../components/SSF/Filter"
import { IFilterGroup } from "../../components/SSF/ModelSSFBar"

const PLAYERS_API_URL = BASE_API_URL + "/players"
const TEAMS_API_URL = BASE_API_URL + "/teams"
const COACHES_API_URL = BASE_API_URL + "/coaches"

const LeagueGroups: IFilterGroup[] = [
  {
    filterOptions: [
      {
        label: "NBA",
        value: "leagueNBA",
      },
      {
        label: "WNBA",
        value: "leagueWNBA",
      },
    ],
    filterLabel: "League Filter",
  },
]

const Search = () => {
  const [searchString, setSearchString] = useState("")
  const [players, setPlayers] = useState<IInstanceData[]>([])
  const [playersTotal, setPlayersTotal] = useState(0)
  const [teams, setTeams] = useState<IInstanceData[]>([])
  const [teamsTotal, setTeamsTotal] = useState(0)
  const [coaches, setCoaches] = useState<IInstanceData[]>([])
  const [coachesTotal, setCoachesTotal] = useState(0)
  const [filters, setFilters] = useState({})

  const [searchParams, setSearchParams] = useSearchParams()

  const [apiUrlLatterHalf, setApiUrlLatterHalf] = useState("")

  useEffect(() => {
    console.log(searchParams)
    if (searchParams.get("search") !== null)
      setSearchString(searchParams.get("search") ?? "")
    const temp = {}
    LeagueGroups.forEach(({ filterLabel }) => (temp[filterLabel] = []))
    if (searchParams.get("filter") !== null) {
      temp["League Filter"] = searchParams.get("filter")?.split(",")
      setFilters(temp)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    const nextSearchParams = {}
    nextSearchParams["search"] = searchString
    if (filters["League Filter"] !== undefined) {
      nextSearchParams["filter"] = filters["League Filter"].join(",")
    }
    let urlLatterHalf = `&search=${nextSearchParams["search"]}`
    if (filters["League Filter"] !== undefined) urlLatterHalf +=`&filter=${filters["League Filter"].join(",")}`
    setSearchParams(nextSearchParams)
    axios.get(`${PLAYERS_API_URL}?page=1${urlLatterHalf}`).then((resp) => {
      setPlayers(resp.data.data)
      setPlayersTotal(resp.data.meta_count)
    })
    axios.get(`${TEAMS_API_URL}?page=1${urlLatterHalf}`).then((resp) => {
      setTeams(resp.data.data)
      setTeamsTotal(resp.data.meta_count)
    })
    axios.get(`${COACHES_API_URL}?page=1${urlLatterHalf}`).then((resp) => {
      setCoaches(resp.data.data)
      setCoachesTotal(resp.data.meta_count)
    })
    setApiUrlLatterHalf(urlLatterHalf)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchString, filters])

  return (
    <Box width="75%" marginX="auto">
      {/* <Box marginTop="3%"> */}
      <Stack direction="row" marginTop="2%" marginBottom="2%">
        <SearchBar
          handleSearchValue={setSearchString}
          enterDownhandler={undefined}
        />
        {LeagueGroups.map((grp) => (
          <Filter
            options={grp.filterOptions}
            filterLabel={grp.filterLabel}
            changeHandler={(lbl: string, arr: string[]) => {
              const newFilers = { ...filters }
              newFilers[lbl] = arr
              setFilters(newFilers)
            }}
          />
        ))}
      </Stack>
      {/* </Box> */}
      <div>
        <h3>Players</h3>
        <Stack direction="row" flexWrap="wrap">
          {players.map((player) => (
            <SearchCard
              modelType={MODEL_TYPE.PLAYER}
              instance={player}
              searchWords={searchString.split(",")}
            />
          ))}
        </Stack>
        {playersTotal > players.length && (
          <SearchMoreButton
            count={playersTotal}
            link={`${BASE_URL}/players?page=1${apiUrlLatterHalf}`}
          />
        )}
      </div>
      <div>
        <h3>Teams</h3>
        <Stack direction="row" flexWrap="wrap">
          {teams.map((team) => (
            <SearchCard
              modelType={MODEL_TYPE.TEAM}
              instance={team}
              searchWords={searchString.split(",")}
            />
          ))}
        </Stack>
        {teamsTotal > teams.length && (
          <SearchMoreButton
            count={teamsTotal}
            link={`${BASE_URL}/teams?page=1${apiUrlLatterHalf}`}
          />
        )}
      </div>
      <div>
        <h3>Coaches</h3>
        <Stack direction="row" flexWrap="wrap">
          {coaches.map((coach) => (
            <SearchCard
              modelType={MODEL_TYPE.COACH}
              instance={coach}
              searchWords={searchString.split(",")}
            />
          ))}
        </Stack>
        {coachesTotal > coaches.length && (
          <SearchMoreButton
            count={coachesTotal}
            link={`${BASE_URL}/coaches?page=1${apiUrlLatterHalf}`}
          />
        )}
      </div>
    </Box>
  )
}

export default Search
