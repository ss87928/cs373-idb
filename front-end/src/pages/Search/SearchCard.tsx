import React from "react"
import Card from "@mui/material/Card"
import CardContent from "@mui/material/CardContent"
import Typography from "@mui/material/Typography"
import { IInstanceData } from "../../common/types"
import { MODEL_TYPE } from "../../common/globalVars"
import {
  TEAM_ID_TO_NAME,
  TYPE_TO_NAME,
} from "../../common/globalVars"
import { CardActionArea } from "@mui/material"
import { Link } from "react-router-dom"
import HText from "../../components/SSF/HText"

interface ISearchCardProps {
  modelType: MODEL_TYPE
  instance: IInstanceData
  searchWords: string[]
}

const SearchCard = ({ modelType, instance, searchWords }: ISearchCardProps) => {
  const {
    id,
    coachid,
    piclink,
    name,
    league,
    conference,
    pos,
    teamid,
    abbreviation,
    state,
  } = instance

  return (

    <Card sx={{ width: "15%"}}>
      <Link
        to={`../${TYPE_TO_NAME(modelType, false)}/${modelType === MODEL_TYPE.COACH ? coachid : id}`}
        style={{ textDecoration: "none" }}
      >
        <CardActionArea>
          <img
            src={piclink}
            alt=""
            style={{ width: "150px", height: "150px" }}
            className="rounded-circle"
          />
          {modelType === MODEL_TYPE.PLAYER && (
            <CardContent>
              <Typography><HText searchWords={searchWords}>{name}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{league}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{pos ?? ""}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{TEAM_ID_TO_NAME[`${teamid}`]}</HText></Typography>
            </CardContent>
          )}
          {modelType === MODEL_TYPE.COACH && (
            <CardContent>
              <Typography><HText searchWords={searchWords}>{name}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{league}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{pos ?? ""}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{TEAM_ID_TO_NAME[`${teamid}`]}</HText></Typography>
            </CardContent>
          )}
          {modelType === MODEL_TYPE.TEAM && (
            <CardContent>
              <Typography><HText searchWords={searchWords}>{name}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{league}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{abbreviation ?? ""}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{conference ?? ""}</HText></Typography>
              <Typography><HText searchWords={searchWords}>{state ?? ""}</HText></Typography>
            </CardContent>
          )}
        </CardActionArea>
      </Link>
    </Card>
  )
}

export default SearchCard
