import { ISortOption } from '../../components/SSF/SortDropdown';
import { IFilterGroup } from '../../components/SSF/ModelSSFBar';

const coachSortOptions:  ISortOption[] = [
  {
    value: "name",
    label: "⬆️ Name"
  },
  {
    value: "@name",
    label: "⬇️ Name"
  },
  {
    value: "careerwins",
    label: "⬆️ Career Wins"
  },
  {
    value: "@careerwins",
    label: "⬇️ Career Wins"
  },
  {
    value: "careerlosses",
    label: "⬆️ Career Losses"
  },
  {
    value: "@careerlosses",
    label: "⬇️ Career Losses"
  },
  {
    value: "careerratio",
    label: "⬆️ Career Win/Loss Ratio"
  },
  {
    value: "@careerratio",
    label: "⬇️ Career Win/Loss Ratio"
  },
  {
    value: "careergames",
    label: "⬆️ Career Games"
  },
  {
    value: "@careergames",
    label: "⬇️ Career Games"
  },
  {
    value: "seasonswithfranchise",
    label: "⬆️ Seasons with Franchise"
  },
  {
    value: "@seasonswithfranchise",
    label: "⬇️ Seasons with Franchise"
  },
  {
    value: "seasonsoverall",
    label: "⬆️ Seasons Overall"
  },
  {
    value: "@seasonsoverall",
    label: "⬇️ Seasons Overall"
  },
  {
    value: "seasongames",
    label: "⬆️ Season Games"
  },
  {
    value: "@seasongames",
    label: "⬇️ Season Games"
  },
  {
    value: "seasonwins",
    label: "⬆️ Season Wins"
  },
  {
    value: "@seasonwins",
    label: "⬇️ Season Wins"
  },
  {
    value: "seasonlosses",
    label: "⬆️ Season Losses"
  },
  {
    value: "@seasonlosses",
    label: "⬇️ Season Losses"
  },
  {
    value: "seasonratio",
    label: "⬆️ Season Win/Loss Ratio"
  },
  {
    value: "@seasonratio",
    label: "⬇️ Season Win/Loss Ratio"
  },
]

const coachFilterGroups: IFilterGroup[] = [
  {
    filterOptions: [
      {
        label: "NBA",
        value: "leagueNBA"
      },
      {
        label: "WNBA",
        value: "leagueWNBA"
      },
    ],
    filterLabel: "League Filter"
  },
  {
    filterOptions: [
      {
        label: "Head Coach",
        value: "posHC"
      },
      {
        label: "Assistant Coach",
        value: "posAC"
      },
    ],
    filterLabel: "Position Filter"
  },
] 

export { coachSortOptions, coachFilterGroups }