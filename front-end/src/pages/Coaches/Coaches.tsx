import React from 'react';
import './Coaches.css'
import ModelPage from '../ModelPage/ModelPage';
import { BASE_API_URL, MODEL_TYPE } from '../../common/globalVars';
import { coachSortOptions, coachFilterGroups } from './CoachOptions';

const Coaches = () =>{
  // TODO: get this data from an API

  return (
    <div className="Coaches">
      <ModelPage 
        baseAPI={`${BASE_API_URL}/coaches`} 
        headers={['League', 'Name', 'Career Games', 'Career Wins', 'Career Losses', 'Career W/L Ratio']}
        type={MODEL_TYPE.COACH}
        sortOptions={coachSortOptions}
        filterGroups={coachFilterGroups}
      />
    </div>
  );
}
export default Coaches;