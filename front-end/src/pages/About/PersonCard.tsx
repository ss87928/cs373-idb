import  {IMember, IMemberStats} from "./AboutTypes"
import Card from 'react-bootstrap/Card'
import "./About.css"
// Card for each member on About page
type IPersonCardProps = {
    member: IMember,
    stats: IMemberStats
};

const AboutCard = ({member, stats}: IPersonCardProps) => {
    return (
      <div className='PersonCard'>
      <Card border="primary" style={{ width: "18rem" }}>
        <Card.Img variant="top" src={member.img_path} />
        <Card.Body>
          <Card.Title>{member.name}</Card.Title>
          <Card.Text>{member.bio}</Card.Text>
          <Card.Text>Responsibilities: {member.responsibilities}</Card.Text>
          <Card.Text>Number of commits: {stats.commits}</Card.Text>
          <Card.Text>Number of issues: {stats.issues}</Card.Text>
          <Card.Text>Number of unit tests: {0}</Card.Text>
        </Card.Body>
      </Card>
    </div>
    );
}

export default AboutCard;
