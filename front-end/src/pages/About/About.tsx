import React, { useState, useEffect } from "react"
import axios from "axios"
import member_tyler from "../../assets/headshots/member_tyler.jpg"
import member_james from "../../assets/headshots/member_james.jpeg"
import member_avi from "../../assets/headshots/member_avi.png"
import member_sri from "../../assets/headshots/member_sri.jpeg"
import member_evelyn from "../../assets/headshots/member_evelyn.jpg"
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import PersonCard from "./PersonCard"
import { IMember, IMemberStats } from "./AboutTypes"

import "./About.css"

const members: IMember[] = [
  {
    name: "Tyler Roberts",
    img_path: member_tyler,
    bio: "I am a senior CS student, likely graduating this December. In my free time, I enjoy watching TV, playing board games, and kayaking.",
    responsibilities: "Mostly worked on front-end.",
  },
  {
    name: 'Sizhan "James" Xu',
    img_path: member_james,
    bio: "I am a senior CS student in the integrated 5-year master program. I enjoy doing sim racing and playing video games.",
    responsibilities: "Mostly worked on front-end.",
  },
  {
    name: "Avinash Gupta",
    img_path: member_avi,
    bio: "I'm a junior studying CS. I love cooking and working out and in my free time I enjoy playing IM volleyball and playing various video and board games.",
    responsibilities: "Mostly worked on front-end and data scraping.",
  },
  {
    name: "Evelyn Vo",
    img_path: member_evelyn,
    bio: "Junior CS student.",
    responsibilities: "Mostly worked on backend and DevOps.",
  },
  {
    name: "Srivatsan Srikanth",
    img_path: member_sri,
    bio: "I am a senior CS student graduating in the Spring of 2023.  I play basketball whenever I'm free and I love the training and hard work associated with it.",
    responsibilities: "Team leader for Phase 1. Mostly worked on front-end.",
  },
]

const USERNAMES = [
  "TylerRoberts01",
  "never0lie",
  "avigupta",
  "evelynnvo",
  "ss87928",
]

const EMAILS = [
  "troberts73001@yahoo.com",
  "sizhan_x@utexas.edu",
  "avigupta@cs.utexas.edu",
  "evelynvo315@gmail.com",
  "ss87928@utexas.edu",
]

interface ICommitsByPerson {
  name: string
  email: string
  commits: number
  additions: number
  deletions: number
}

interface IIssuesByPerson {
  iid: string
  title: string
  author: string
}

/**
 * The component that is resposible for fetching all the necessary data from
 * Gitlab
 *
 */
const About = () => {
  const [commits, setCommits] = useState<ICommitsByPerson[]>([])
  const [issues, setIssues] = useState<IIssuesByPerson[]>([])

  useEffect(() => {
    axios
      .get(
        "https://gitlab.com/api/v4/projects/39551908/repository/contributors",
      )
      .then((res) => setCommits(res.data))
    axios
      .get("https://gitlab.com/api/v4/projects/39551908/issues?per_page=100")
      .then((res) =>
        setIssues(
          res.data.map((issue) => {
            return {
              iid: issue.iid,
              title: issue.title,
              author: issue.author.username,
            }
          }),
        ),
      )
  }, [])

  const statsGetter = (index: number) => {
    let individualCommitsCount = 0
    const induvidualCommits = commits.find((cmt) => cmt.email === EMAILS[index])
    if (induvidualCommits !== undefined) {
      individualCommitsCount = induvidualCommits.commits
    }
    return {
      commits: individualCommitsCount,
      issues: issues.filter((issue) => issue.author === USERNAMES[index])
        .length,
    } as IMemberStats
  }

  return (
    <div>
      <h3 className="Header">About</h3>
      <Description />
      <h3 className="Header">Meet the Team</h3>
      <div className="PersonGrid">
        <Container className="d-grid gap-3">
          <Row xs={6} className="d-flex justify-content-evenly">
            <Col>
              <PersonCard data-testid="about-person-p" member={members[0]} stats={statsGetter(0)} />
            </Col>
            <Col>
              <PersonCard data-testid="about-person-p" member={members[1]} stats={statsGetter(1)} />
            </Col>
            <Col>
              <PersonCard data-testid="about-person-p" member={members[2]} stats={statsGetter(2)} />
            </Col>
          </Row>
          <Row xs={6} className="d-flex justify-content-evenly">
            <Col>
              <PersonCard data-testid="about-person-p" member={members[3]} stats={statsGetter(3)} />
            </Col>
            <Col>
              <PersonCard data-testid="about-person-p" member={members[4]} stats={statsGetter(4)} />
            </Col>
          </Row>
        </Container>
      </div>
      <div className="BottomText">
        Number of total commits:{" "}
        {commits.map((person) => person.commits).reduce((a, b) => a + b, 0)}
        <br />
        Number of total issues: {issues.length}
        <br />
        Number of total unit tests: 0
      </div>
      <h3 className="Header">Links</h3>
      <div className="Links">
        <div className="Link">
          <li>
            <a
              href="https://www.basketball-reference.com/"
              target="_blank"
              rel="noreferrer"
            >
              Basketball Reference
            </a>
            &nbsp;&nbsp;NBA and WNBA related data
          </li>
          <li>
            <a href="https://www.nba.com/" target="_blank" rel="noreferrer">
              NBA
            </a>
            &nbsp;&nbsp;NBA data
          </li>
          <li>
            <a
              href="https://www.balldontlie.io/#get-all-players"
              target="_blank"
              rel="noreferrer"
            >
              Ball Don't Lie
            </a>
            &nbsp;&nbsp;NBA data
          </li>
          <li>
            <a
              href="https://developer.sportradar.com/docs/read/basketball/NBA_G_League_v7"
              target="_blank"
              rel="noreferrer"
            >
              Sport Rader
            </a>
            &nbsp;&nbsp;G-League Data
          </li>
          <li>
            <a href="https://documenter.getpostman.com/view/23610428/2s83meo44X"
            target="_blank"
            rel="noreferrer"
            >
              Lowball API Documentation
            </a>
          </li>
          <li>
            <a href="https://gitlab.com/lowball/cs373-idb"
            target="_blank"
            rel="noreferrer"
            >
              Lowball Source Code
            </a>
          </li>
        </div>
      </div>
      <h3 className="Header">Tools</h3>
      <Tools />
    </div>
  )
}

const Description = () => {
  return (
    <div className="Description">
      Welcome to Low-ball.me! Our purpose is to combine basketball data from
      various sources to create an easily accessible website where anyone
      interested in the sport can see and compare stats and salaries of
      different players, coaches, and teams. We are particularly interested in
      finding any discrimination present in the salaries of different players.
    </div>
  )
}

const Tools = () => {
  return (
    <div className="Tools">
      <div className="Tool">
        <b>React</b>: A front-end JavaScript library that our team used to build
        the base front-end of our website.
      </div>
      <div className="Tool">
        <b>Typescript/Typescript-ESLint</b>: A superset of Javascript that
        enforces strict syntax. We are using this to maintain the readability of
        our code.
      </div>
      <div className="Tool">
        <b>React-Bootstrap</b>: An open-source CSS framework implemented using
        React that replaces the original Bootstrap for JavaScript.
      </div>
      <div className="Tool">
        <b>Postman</b>: An API platform used to design, build, test, and iterate
        over APIs. For phase one, we are using Postman to host the documentation
        for our API.
      </div>
      <div className="Tool">
        <b>Namecheap</b>: A domain name registrar we used to register a free
        domain for our website.
      </div>
      <div className="Tool">
        <b>AWS Amplify</b>: AWS’s web hosting service. This is what we used to
        host our website.
      </div>
      <div className="Tool">
        <b>GitLab</b>: A web-based version control tool that enables source code
        management, development, and collaboration. GitLab offers a complete
        suite of DevOps tools such as a CI/CD pipeline and issue tracking. Our
        team used GitLab to hold all our source code, automatically build our
        code, and create issues to track tasks and bugs for our project.
      </div>
      <div className="Tool">
        <b>Discord</b>: A popular communication platform that our team used to
        coordinate meeting times and tasking.
      </div>
      <div className="Tool">
        <b>Yarn</b>: A package manager our team is using to build and run our
        web application.{" "}
      </div>
    </div>
  )
}

export default About
