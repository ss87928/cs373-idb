interface IMember {
  name: string,
  img_path: string,
  bio: string,
  responsibilities: string,
}

interface IMemberStats {
  commits: number,
  issues: number
}

export type {IMember, IMemberStats}