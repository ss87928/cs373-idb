import { Typography, Container, Stack } from "@mui/material";
import BarChartVisualization from "./BarChart";
import PieChartVisualization from "./PieChart";
import RadarChartVisualization from "./RadarChart";

function VisualizationPage(props: any) {
  return (
    <Container className="page-container" sx={{ textAlign: "center" }}>
      <Stack justifyContent="center" direction="column" textAlign="center">
        <Typography className="modelTitle" variant="h2" gutterBottom>
          Visualizations
        </Typography>
        <Typography
          gutterBottom
          sx={{ marginTop: "16px", marginBottom: "8px" }}
          variant="h4"
        >
          Salary of Male and Female Players by PPG
        </Typography>
        <BarChartVisualization />
        <Typography
          gutterBottom
          sx={{ marginTop: "48px", marginBottom: "8px" }}
          variant="h4"
        >
          League Win Rate by State
        </Typography>
        <PieChartVisualization />
        <Typography
          gutterBottom
          sx={{ marginTop: "48px", marginBottom: "8px" }}
          variant="h4"
        >
            Player Performance Comparison
        </Typography>
        <RadarChartVisualization />
      </Stack>
    </Container>
  );
}

export default VisualizationPage;
