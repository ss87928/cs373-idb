import React, { useEffect, useState} from "react";
import axios from "axios";
import { IInstanceData } from "../../common/types";
import * as d3 from "d3";
import { BASE_API_URL } from '../../common/globalVars';
import {
    Bar,
    BarChart,
    CartesianGrid,
    Label,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis
  } from "recharts";

  const BarChartVisualization = () => {
    var [data, setData] = useState<any[]>([]);
    var [instances, setInstances] = useState<IInstanceData[]>([])
  
    useEffect(() => {
        let url = BASE_API_URL + "/players"

        axios.get(url).then((res) => {
          console.log(res.data.data)
          setInstances(res.data.data)
        });
      }, []);

    useEffect(() => {
        var map = new Map();
        map.set("0-10", {men: [], women: []});
        map.set("10-20", {men: [], women: []});
        map.set("20-30+", {men: [], women: []});

        instances.forEach(d => {
            var ppg = d.ppg
            var gender = d.gender
            var salary = d.salary_int
            if(ppg !== undefined && gender !== undefined) {
                if(0 <= ppg && ppg < 10) {
                    if(gender === "F") {
                        map.get("0-10").women.push(salary);
                    } else if(gender === "M") {
                        map.get("0-10").men.push(salary);
                    }
                } else if(10 <= ppg && ppg < 20) {
                    if(gender === "F") {
                        map.get("10-20").women.push(salary);
                    } else if(gender === "M") {
                        map.get("10-20").men.push(salary);
                    }
                } else if(20 <= ppg) {
                    if(gender === "F") {
                        map.get("20-30+").women.push(salary);
                    } else if(gender === "M") {
                        map.get("20-30+").men.push(salary);
                    }
                }
            }
        });


        setData([
            {
                name: '0 - 10',
                men: d3.mean(map.get("0-10").men),
                women: d3.mean(map.get("0-10").women)
            },
            {
                name: '10 - 20',
                men: d3.mean(map.get("10-20").men),
                women: d3.mean(map.get("10-20").women)
            },
            {
                name: '20 - 30+',
                men: d3.mean(map.get("20-30+").men),
                women: d3.mean(map.get("20-30+").women)
            }
        ]);
    }, [instances]);
  
    return (
        <ResponsiveContainer width="100%" height={400}>
            <BarChart
            data={data}
            margin={{
                top: 20,
                right: 30,
                left: 40,
                bottom: 5,
            }}
            >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" tick={true}>
                <Label
                value="Points per game"
                position="insideBottom"
                style={{ textAnchor: "middle" }}
                />
            </XAxis>
            <YAxis tick={false}>
            
                <Label
                angle={-90}
                value="Salary in dollars"
                position="insideLeft"
                style={{ textAnchor: "middle" }}
                />
            </YAxis>
            <Tooltip payload={data} formatter = {(value) => value && value.toLocaleString("en-US", {style:"currency", currency:"USD"})} />
            <Bar dataKey="women" fill="#8884d8" />
            <Bar dataKey="men" fill="#82ca9d" />
            </BarChart>
        </ResponsiveContainer>
    );
  }
  
  export default BarChartVisualization;
  