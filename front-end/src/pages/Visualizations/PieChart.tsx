import { TextField, MenuItem, Box, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { IInstanceData } from "../../common/types";
import { BASE_API_URL } from '../../common/globalVars';
import {
    ResponsiveContainer,
    PieChart,
    Pie,
    Tooltip,
    Cell
} from "recharts";


const PieChartVisualization = () => {
    let [currentState, setCurrentState] = React.useState<string>("Arizona");
    let [instances, setInstances] = useState<IInstanceData[]>([]);
    var [nbaData, setNbaData] = useState<any[]>([]);
    var [wnbaData, setWnbaData] = useState<any[]>([]);

    var stateOptions = [
        'Arizona',
        'California', 'Colorado', 'Connecticut',
        'Florida', 'Georgia',
        'Illinois', 'Indiana',
        'Louisiana','Massachusetts', 'Michigan',
        'Minnesota','Nevada', 'New York', 'North Carolina',
        'Ohio', 'Oklahoma', 'Oregon',
        'Pennsylvania','Tennessee', 'Texas', 'Utah',
        'Washington', 'Wisconsin'
    ]

    useEffect(() => {
        let url = BASE_API_URL + "/teams"


        axios.get(url).then((res) => {
            console.log(res.data.data)
            setInstances(res.data.data)
        })
    }, []);

    useEffect(() => {
        var map = new Map();
        map.set("NBA", { wins: 0, losses: 0 });
        map.set("WNBA", { wins: 0, losses: 0 });

        instances.forEach(d => {
            var state = d.state
            var league = d.league
            var wins = d.wins
            var losses = d.losses
            if (state !== undefined && league !== undefined && wins !== undefined && losses !== undefined) {
                if (state === currentState) {
                    if (league === 'NBA') {
                        map.get('NBA').wins += wins
                        map.get('NBA').losses += losses
                    } else if (league === 'WNBA') {
                        map.get('WNBA').wins += wins
                        map.get('WNBA').losses += losses
                    }
                }
            }
        });

        setNbaData([
            { name: "Wins", value: map.get("NBA").wins },
            { name: "Losses", value: map.get("NBA").losses }
        ])

        setWnbaData([
            { name: "Wins", value: map.get("WNBA").wins },
            { name: "Losses", value: map.get("WNBA").losses }
        ])
    }, [currentState, instances]);

    const NBA_COLORS = ['#82ca9d', '#4d785d'];
    const WNBA_COLORS = ['#8884d8', '#4c4985'];

    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);

        return (
            <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };

    return (
        <Box marginTop="16px">
            <TextField
                id="filter-field"
                select
                value={currentState}
                onChange={(event) => setCurrentState(event.target.value)}
                InputProps={{
                    sx: {
                        borderRadius: "8px",
                        backgroundColor: "white",
                        flexGrow: 1,
                        minWidth: "150px",
                        display: "flex",
                        textAlign: "start",
                    },
                }}
            >
                {stateOptions.map((option) => (
                    <MenuItem key={option} value={option}>
                        {option}
                    </MenuItem>
                ))}
            </TextField>
            <Grid justifyContent="center" container spacing={1}>
                <Grid item xs={6}>
                    <ResponsiveContainer width="100%" height={300}>
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="value"
                                data={wnbaData}
                                outerRadius={130}
                                fill="#8884d8"
                                labelLine={false}
                                label={renderCustomizedLabel}
                            >
                                {wnbaData.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={WNBA_COLORS[index % WNBA_COLORS.length]} />
                                ))}
                            </Pie>
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Grid>
                <Grid item xs={6}>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="value"
                                data={nbaData}
                                outerRadius={130}
                                fill="#82ca9d"
                                labelLine={false}
                                label={renderCustomizedLabel}
                            >
                                {nbaData.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={NBA_COLORS[index % NBA_COLORS.length]} />
                                ))}
                            </Pie>
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Grid>
                <Grid item xs={6}>
                    <h1>WNBA</h1>
                </Grid>
                <Grid item xs={6}>
                    <h1>NBA</h1>
                </Grid>
            </Grid>
        </Box>
    );
}

export default PieChartVisualization;