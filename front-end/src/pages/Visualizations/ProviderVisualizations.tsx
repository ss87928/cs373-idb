import { Typography, Container, Stack } from "@mui/material";
import {DifficultyTypeVisualization, PriceVisualization, RoutineLengthVisualizaion} from "./WorkItOutChart";

function ProviderVisualizationPage(props: any) {
  return (
    <Container className="page-container" sx={{ textAlign: "center" }}>
      <Stack justifyContent="center" direction="column" textAlign="center">
        <Typography
          gutterBottom
          sx={{ marginTop: "48px", marginBottom: "8px" }}
          variant="h4"
        >
            WorkItOut: Distribution of Forces over Levels
        </Typography>
        <DifficultyTypeVisualization />
        <Typography
          gutterBottom
          sx={{ marginTop: "48px", marginBottom: "8px" }}
          variant="h4"
        >
            WorkItOut: Distribution of Equipment Price by Highest Price 
        </Typography>
        <PriceVisualization />
        <Typography
          gutterBottom
          sx={{ marginTop: "48px", marginBottom: "8px" }}
          variant="h4"
        >
            WorkItOut: Distribution of Routine Length 
        </Typography>
        <RoutineLengthVisualizaion />
      </Stack>
    </Container>
  );
}

export default ProviderVisualizationPage;
