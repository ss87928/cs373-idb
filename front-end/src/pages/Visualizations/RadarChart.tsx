import React, { useEffect, useState } from "react";
import { TextField, Box, Grid, Autocomplete } from "@mui/material";
import axios from "axios";
import { BASE_API_URL } from '../../common/globalVars';
import {
  Legend,
  ResponsiveContainer,
  RadarChart,
  Radar,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
} from "recharts";
import { IInstanceData } from "../../common/types";

const RadarChartVisualization = () => {
    let [players, setPlayers] = useState<IInstanceData[]>([]);
    let [teams, setTeams] = useState<IInstanceData[]>([]);
    let [currentPlayerOne, setCurrentPlayerOne] = React.useState<string>("Player A");
    let [currentPlayerTwo, setCurrentPlayerTwo] = React.useState<string>("Player B");
    let playerMap = new Map<string, IInstanceData>();
    let teamIdMap = new Map<string, IInstanceData>();

    var [data, setData] = useState<any[]>([]);
    var playerNames : string[] = []

    useEffect(() => {
        let url = BASE_API_URL

        axios.get(url + "/players?sort=name").then((res) => {
          console.log(res.data.data)
          setPlayers(res.data.data)
        });

        axios.get(url + "/teams").then((res) => {
          console.log(res.data.data)
          setTeams(res.data.data)
        });
    }, []);

    teams.forEach(d => {
        teamIdMap.set(d.id, d)
    });

    players.forEach(d => {
        var teamId = d.teamid
        if(teamId !== undefined) {
            var team = teamIdMap.get(teamId)
            if(team !== undefined) {
                var key = d.name + " (" + team.league + " " + team.name + ")"
                playerNames.push(key)
                playerMap.set(key, d)
            }
        }
    });
    
    const getPlayerSalary = (player) => {
        var playerData = playerMap.get(player);
        if(playerData !== undefined) {
            return (
                playerData.salary_int.toLocaleString("en-US", {style:"currency", currency:"USD"})
            );
        }

    }
        

    useEffect(() => {
        console.log(currentPlayerOne)
        console.log(currentPlayerTwo)
        var map = new Map();

        const p1Apg = playerMap.get(currentPlayerOne)?.apg;
        const p1Ppg = playerMap.get(currentPlayerOne)?.ppg;
        const p1Rpg = playerMap.get(currentPlayerOne)?.rpg;

        const p2Apg = playerMap.get(currentPlayerTwo)?.apg;
        const p2Ppg = playerMap.get(currentPlayerTwo)?.ppg;
        const p2Rpg = playerMap.get(currentPlayerTwo)?.rpg;
    
        map.set("apg", {A: p1Apg, B: p2Apg, fullMark: 31});
        map.set("ppg", {A: p1Ppg, B: p2Ppg, fullMark: 31});
        map.set("rpg", {A: p1Rpg, B: p2Rpg, fullMark: 31});

        setData([
            {
                stat: "APG", 
                A: map.get("apg").A,
                B: map.get("apg").B,
                fullMark: map.get("apg").fullMark
            },
            {
                stat: "PPG", 
                A: map.get("ppg").A,
                B: map.get("ppg").B,
                fullMark: map.get("ppg").fullMark
            },{
                stat: "RPG", 
                A: map.get("rpg").A,
                B: map.get("rpg").B,
                fullMark: map.get("rpg").fullMark
            }
        ]);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentPlayerOne, currentPlayerTwo]);

    return(
        <Box marginTop="16px">
            <Grid justifyContent="center" container spacing={1}>
                <Grid item xs={6}>
                <Autocomplete
                    disablePortal
                    id="combo-box-demo"
                    options={playerNames}
                    sx={{ width: 500 }}
                    onChange={(event: any, newValue: string | null) => {
                        if(newValue !== null) {
                            setCurrentPlayerOne(newValue);
                        }
                      }}
                      renderInput={(params) => <TextField {...params} label="Player A" placeholder="Player A" />}
                    />
                </Grid>
                <Grid item xs={6}>
                <Autocomplete
                    disablePortal
                    id="combo-box-demo"
                    options={playerNames}
                    sx={{ width: 500 }}
                    onChange={(event: any, newValue: string | null) => {
                        if(newValue !== null) {
                            setCurrentPlayerTwo(newValue);
                        }
                      }}
                    renderInput={(params) => <TextField {...params} label="Player B" placeholder="Player B" />}
                    />
                </Grid>
                <Grid item xs={6}>
                      <h3>
                        Salary: {getPlayerSalary(currentPlayerOne)}
                      </h3>
                </Grid>
                <Grid item xs={6}>
                    <h3>
                    Salary: {getPlayerSalary(currentPlayerTwo)}
                    </h3>
                </Grid>
                <Grid item xs={12}>
                <ResponsiveContainer width="100%" height={500}>
                    <RadarChart outerRadius={150} width={730} height={300} data={data}>
                        <PolarGrid />
                        <PolarAngleAxis dataKey="stat" />
                        <PolarRadiusAxis angle={90} domain={[0, 32]} />
                        <Radar
                        name={currentPlayerOne}
                        dataKey="A"
                        stroke="#8884d8"
                        fill="#8884d8"
                        fillOpacity={0.6}
                        />
                        <Radar
                        name={currentPlayerTwo}
                        dataKey="B"
                        stroke="#82ca9d"
                        fill="#82ca9d"
                        fillOpacity={0.6}
                        />
                        <Legend />
                    </RadarChart>
                    </ResponsiveContainer>
                </Grid>
            </Grid>
        </Box>
    );
}

export default RadarChartVisualization

