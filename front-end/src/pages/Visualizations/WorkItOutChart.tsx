import React, { useEffect, useState } from "react"
import axios from "axios"
import * as d3 from "d3"
import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
  Legend,
  PieChart,
  Pie,
  Cell,
  LineChart,
  Line
} from "recharts"

interface IExerciseData {
  category: string
  equipment_id: number
  equipment_name: string
  force: string
  id: number
  images: Object[]
  instructions: string
  level: string
  mechanic: string
  muscles_primary: Object[]
  muscles_secondary: Object[]
  name: string
  videos: Object[]
}

const RADIAN = Math.PI / 180;

let WIO_API_URL = "https://api.workit0ut.me"

const DifficultyTypeVisualization = () => {
  var [data, setData] = useState<any[]>([])
  var [instances, setInstances] = useState<IExerciseData[]>([])

  useEffect(() => {
    let url = WIO_API_URL + "/exercises"

    axios.get(url).then((res) => {
      console.log(res.data.Exercises)
      setInstances(res.data.Exercises)
    })
  }, [])

  useEffect(() => {
    var map = new Map()
    map.set("beginner", { static: [], push: [], pull: [] })
    map.set("intermediate", { static: [], push: [], pull: [] })
    map.set("expert", { static: [], push: [], pull: [] })

    instances.forEach((d) => {
      var level = d.level
      var force = d.force
      if (level !== undefined && force !== undefined) {
        if (force === "static") {
          map.get(level).static.push(1)
        } else if (force === "push") {
          map.get(level).push.push(1)
        } else if (force === "pull") {
          map.get(level).pull.push(1)
        }
      }
    })

    setData([
      {
        name: "beginner",
        static: d3.sum(map.get("beginner").static),
        push: d3.sum(map.get("beginner").push),
        pull: d3.sum(map.get("beginner").pull),
      },
      {
        name: "intermediate",
        static: d3.sum(map.get("intermediate").static),
        push: d3.sum(map.get("intermediate").push),
        pull: d3.sum(map.get("intermediate").pull),
      },
      {
        name: "expert",
        static: d3.sum(map.get("expert").static),
        push: d3.sum(map.get("expert").push),
        pull: d3.sum(map.get("expert").pull),
      },
    ])
  }, [instances])

  return (
    <ResponsiveContainer width="100%" height={400}>
      <BarChart
        data={data}
        margin={{
          top: 20,
          right: 30,
          left: 40,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" tick={true}>
          <Label
            value="Level"
            position="insideBottom"
            style={{ textAnchor: "middle" }}
            offset={-5}
          />
        </XAxis>
        <YAxis tick={false}>
          <Label
            angle={-90}
            value="Number of Exercises"
            position="insideLeft"
            style={{ textAnchor: "middle" }}
          />
        </YAxis>
        <Tooltip />
        <Legend />
        <Bar dataKey="static" fill="#33ff3f" />
        <Bar dataKey="push" fill="#335bff" />
        <Bar dataKey="pull" fill="#ff3333" />
      </BarChart>
    </ResponsiveContainer>
  )
}

interface IPVRenderData {
  name: string
  value: number
}

const PriceVisualization = () => {
  const [pricesData, setPricesData] = useState<number[]>([])
  const [renderData, setRenderData] = useState<IPVRenderData[]>([])

  useEffect(() => {
    let temp = Array(25)
    temp.fill(0)
    axios
      .get("https://api.workit0ut.me/equipment")
      .then((resp) => resp.data.Equipment)
      .then((data) =>
        data.map((d) => ({
          low: d["price_low"],
          high: d["price_high"],
        })),
      )
      .then((data) => {
        for (let { high } of data) {
          if (high === 10000) {
            temp[24] += 1
          } else {
            temp[Math.floor(high / 400)] += 1
          }
        }
        setPricesData(temp)
      })
  }, [])

  useEffect(() => {
    const temp2 = pricesData.map((val, idx) => ({
      name: `${idx * 400}~${(idx + 1) * 400}`,
      value: val,
    }))
    setRenderData(temp2)
  }, [pricesData])

  return (
    <ResponsiveContainer width="100%" height={400}>
      <LineChart
        data={renderData}
        margin={{
          top: 20,
          right: 30,
          left: 40,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name">
          <Label
            value="Price Range"
            position="insideBottom"
            style={{ textAnchor: "middle" }}
            offset={-5}
          />
        </XAxis>
        <YAxis>
          <Label
            angle={-90}
            value="Number of Equipments"
            position="insideLeft"
            style={{ textAnchor: "middle" }}
          />
        </YAxis>
        <Tooltip />
        <Legend />
        <Line dataKey="value" fill="#ff3333" />
      </LineChart>
    </ResponsiveContainer>
  )
}

const RoutineLengthVisualizaion = () => {
  const [routineData, setRoutineData] = useState<number[]>([])
  const [renderData, setRenderData] = useState<IPVRenderData[]>([])

  useEffect(() => {
    axios
      .get("https://api.workit0ut.me/routines")
      .then((resp) => resp.data.Routines)
      .then((routines) => routines.map((routine) => routine.length))
      .then((routineLengths) => setRoutineData(routineLengths))
  }, [])

  useEffect(() => {
    const tempRenderData = [
      {
        name: "0~15",
        value: 0,
      },
      {
        name: "16~30",
        value: 0,
      },
      {
        name: "31~45",
        value: 0,
      },
      {
        name: "46~60",
        value: 0,
      },
    ]

    for (let length of routineData) {
      if (length <= 15) {
        tempRenderData[0].value += 1
      } else if (length <= 30) {
        tempRenderData[1].value += 1
      } else if (length <= 45) {
        tempRenderData[2].value += 1
      } else {
        tempRenderData[3].value += 1
      }
    }

    setRenderData(tempRenderData)
  }, [routineData])

  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index,
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5
    const x = cx + radius * Math.cos(-midAngle * RADIAN)
    const y = cy + radius * Math.sin(-midAngle * RADIAN)

    return (
      <text
        x={x}
        y={y}
        fill="white"
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    )
  }

  return (
    <ResponsiveContainer width="100%" height={500}>
      <PieChart>
        <Pie data={renderData} dataKey="value" 
        label={renderCustomizedLabel}
        >
          <Cell fill="red"/>
          <Cell fill="green"/>
          <Cell fill="orange"/>
          <Cell fill="blue"/>
        </Pie>
        <Legend />
        <Tooltip />
      </PieChart>
    </ResponsiveContainer>
  )
}

export {
  DifficultyTypeVisualization,
  PriceVisualization,
  RoutineLengthVisualizaion,
}
