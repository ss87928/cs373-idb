import React from 'react';
import './ErrorPage.css';

const ErrorPage = () =>{
  return (
    <div className="ErrorPage">

      <header className="Error-header">
        <h1> 404 Error: Page Not Found </h1>
        <p> The page you are looking for does not exist! Please redirect.</p> 
      </header>
    </div>
  );
}

export default ErrorPage;
