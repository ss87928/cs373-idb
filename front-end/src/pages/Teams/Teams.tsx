import React from 'react';
import './Teams.css'
import ModelPage from '../ModelPage/ModelPage';
import { BASE_API_URL, MODEL_TYPE } from '../../common/globalVars';
import { teamSortOptions, teamFilterGroups } from './TeamOptions';


const Teams = () =>{
  // TODO: get this data from an API

  return (
    <div className="Teams">
      <ModelPage 
        baseAPI={`${BASE_API_URL}/teams`} 
        headers={['League', 'Name', 'Abbreviation', 'Games Played', 'Wins', 'Losses', 'W/L Ratio', "Rank", "Championships", "Conference", "State"]}
        type={MODEL_TYPE.TEAM}  
        sortOptions={teamSortOptions}
        filterGroups={teamFilterGroups}
      />
    </div>
  );
}
export default Teams;