import { ISortOption } from '../../components/SSF/SortDropdown';
import { IFilterGroup } from '../../components/SSF/ModelSSFBar';
import { states } from '../../common/states';

const teamSortOptions:  ISortOption[] = [
  {
    value: "name",
    label: "⬆️ Name"
  },
  {
    value: "@name",
    label: "⬇️ Name"
  },
  {
    value: "gp",
    label: "⬆️ Games Played"
  },
  {
    value: "@gp",
    label: "⬇️ Games Played"
  },
  {
    value: "wins",
    label: "⬆️ Wins"
  },
  {
    value: "@wins",
    label: "⬇️ Wins"
  },
  {
    value: "losses",
    label: "⬆️ Lossses"
  },
  {
    value: "@losses",
    label: "⬇️ Losses"
  },
  {
    value: "ratio",
    label: "⬆️ Win/Loss Ratio"
  },
  {
    value: "@ratio",
    label: "⬇️ Win/Loss Ratio"
  },
  {
    value: "rank",
    label: "⬆️ Rank"
  },
  {
    value: "@rank",
    label: "⬇️ Rank"
  },
  {
    value: "championships",
    label: "⬆️ Championships"
  },
  {
    value: "@championships",
    label: "⬇️ Championships"
  },
]

const teamFilterGroups: IFilterGroup[] = [
  {
    filterOptions: [
      {
        label: "NBA",
        value: "leagueNBA"
      },
      {
        label: "WNBA",
        value: "leagueWNBA"
      },
    ],
    filterLabel: "League Filter"
  },
  {
    filterOptions: [
      {
        label: "NBA East",
        value: "conferenceNBAEast"
      }, 
      {
        label: "NBA West",
        value: "conferenceNBAWest"
      }, 
      {
        label: "WNBA East",
        value: "conferenceWNBAEast"
      }, 
      {
        label: "WNBA West",
        value: "conferenceWNBAWest"
      }, 
    ],
    filterLabel: "Conference Filter"
  },
  {
    filterOptions: states,
    filterLabel: "State Filter"
  },
]

export {teamSortOptions, teamFilterGroups}