#!/usr/bin/env bash

# code citation: team Get That Bread at https://gitlab.com/Nathaniel-Nemenzo/getthatbread

service nginx start
uwsgi --ini uwsgi.ini