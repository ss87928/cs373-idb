# code citation: team Get That Bread at https://gitlab.com/Nathaniel-Nemenzo/getthatbread

import os
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
CORS(app)

"""
RDS CONFIG
"""
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://{RDS_USERNAME}:{RDS_PASSWORD}@{RDS_HOSTNAME}:{RDS_PORT}/{RDS_DB_NAME}".format(
    RDS_USERNAME="postgres",
    RDS_PASSWORD="lowballer",
    RDS_HOSTNAME="idb-postgres.cudzfopfxcio.us-east-1.rds.amazonaws.com",
    RDS_PORT=5432,
    RDS_DB_NAME="postgres",
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)


class Player(db.Model):
    __tablename__ = "players"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    league = db.Column(db.String())
    gp = db.Column(db.Integer())
    ppg = db.Column(db.Float())
    apg = db.Column(db.Float())
    rpg = db.Column(db.Float())
    piclink = db.Column(db.String())
    gender = db.Column(db.String())
    pos = db.Column(db.String())
    teamid = db.Column(db.String())
    youtubelink = db.Column(db.String())

    # adjust in db
    salary = db.Column(db.String())

    salary_int = db.Column(db.Integer())


class Coach(db.Model):
    __tablename__ = "coaches"
    coachid = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    league = db.Column(db.String())

    # adjust columns in database
    careerwins = db.Column(db.Integer())
    careerlosses = db.Column(db.Integer())
    careerratio = db.Column(db.Float())
    careergames = db.Column(db.Integer())

    piclink = db.Column(db.String())
    teamid = db.Column(db.String())

    # add columns to database
    pos = db.Column(db.String())
    seasonswithfranchise = db.Column(db.Integer())
    seasonsoverall = db.Column(db.Integer())
    seasongames = db.Column(db.Integer())
    seasonwins = db.Column(db.Integer())
    seasonlosses = db.Column(db.Integer())
    seasonratio = db.Column(db.Float())
    youtubelink = db.Column(db.String())


class Team(db.Model):
    __tablename__ = "teams"
    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())
    league = db.Column(db.String())
    gp = db.Column(db.Integer())
    wins = db.Column(db.Integer())
    losses = db.Column(db.Integer())
    ratio = db.Column(db.Float())
    piclink = db.Column(db.String())
    rank = db.Column(db.Integer())
    abbreviation = db.Column(db.String())
    conference = db.Column(db.String())
    championships = db.Column(db.Integer())
    state = db.Column(db.String())
    youtubelink = db.Column(db.String())
