# code citation: team Get That Bread at https://gitlab.com/Nathaniel-Nemenzo/getthatbread
from sqlalchemy import inspect


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}


def get_query(key, queries):
    try:
        return queries[key]
    except KeyError:
        return None


def player_to_dict(obj):
    d = {}
    fields = (
        "id",
        "name",
        "league",
        "gp",
        "ppg",
        "apg",
        "rpg",
        "piclink",
        "gender",
        "pos",
        "teamid",
        "salary",
        "coachid",
        "youtubelink",
    )
    p = iter(fields)
    for v in obj:
        d[next(p)] = v

    return d
