# code citation: team Get That Bread at https://gitlab.com/Nathaniel-Nemenzo/getthatbread

from db.models import app
from flask_marshmallow import Marshmallow, fields, Schema

ma = Marshmallow(app)


class PlayerSchema(Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "league",
            "gp",
            "ppg",
            "apg",
            "rpg",
            "piclink",
            "gender",
            "pos",
            "teamid",
            "salary",
            "coachid",
            "youtubelink",
            "salary_int"
        )


class CoachSchema(Schema):
    class Meta:
        fields = (
            "coachid",
            "name",
            "league",
            "careergames",
            "careerwins",
            "careerlosses",
            "careerratio",
            "seasonswithfranchise",
            "seasonsoverall",
            "seasongames",
            "seasonwins",
            "seasonlosses",
            "seasonratio",
            "piclink",
            "teamid",
            "featuredplayerid",
            "pos",
            "youtubelink",
        )


class TeamSchema(Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "league",
            "gp",
            "wins",
            "losses",
            "ratio",
            "piclink",
            "rank",
            "abbreviation",
            "conference",
            "championships",
            "state",
            "allplayerids",
            "coachid",
            "youtubelink",
        )


class PlayerSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "league",
            "gp",
            "ppg",
            "apg",
            "rpg",
            "piclink",
            "gender",
            "pos",
            "teamid",
            "salary",
            "coachid",
            "youtubelink",
            "salary_int"
        )


class CoachSchema(ma.Schema):
    class Meta:
        fields = (
            "coachid",
            "name",
            "league",
            "careergames",
            "careerwins",
            "careerlosses",
            "careerratio",
            "seasonswithfranchise",
            "seasonsoverall",
            "seasongames",
            "seasonwins",
            "seasonlosses",
            "seasonratio",
            "piclink",
            "teamid",
            "featuredplayerid",
            "pos",
            "youtubelink",
        )


class TeamSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "league",
            "gp",
            "wins",
            "losses",
            "ratio",
            "piclink",
            "rank",
            "abbreviation",
            "conference",
            "championships",
            "state",
            "allplayerids",
            "coachid",
            "youtubelink",
        )


player_schema = PlayerSchema()
coach_schema = CoachSchema()
team_schema = TeamSchema()

player_schema = PlayerSchema(many=True)
coach_schema = CoachSchema(many=True)
team_schema = TeamSchema(many=True)
