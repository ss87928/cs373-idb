# code citation: https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/blob/main/backend/db/job.py

from .models import Team, Player, Coach, db as database
from .schemas import team_schema
from sqlalchemy import or_
from .helpers import get_query, object_as_dict
import re


searchable_attributes = ["name", "league", "abbreviation", "conference", "state"]


def team_handler(team_id):
    team = Team.query.get(team_id)
    d = object_as_dict(team)

    coach = (
        database.session.query(
            Coach.__table__.columns["name"], Coach.__table__.columns["coachid"]
        )
        .filter(Coach.teamid == team_id)
        .filter(Coach.pos == "Head Coach")
        .all()
    )
    coach_data = {}
    coach_data["coachid"] = coach[0][1]
    coach_data["name"] = coach[0][0]

    d["coach"] = coach_data

    players = (
        database.session.query(
            Player.__table__.columns["name"], Player.__table__.columns["id"]
        )
        .filter(Player.teamid == team_id)
        .all()
    )
    players_list = []
    for t in players:
        _d = {}
        _d["name"] = t[0]
        _d["id"] = t[1]
        players_list.append(_d)

    d["players"] = players_list
    return d



def sort_team_by(sorting_string, query, desc):
    q = None
    try:
        q = getattr(Team, sorting_string)
    except:
        return query

    if desc:
        return query.order_by(q.desc())
    else:
        return query.order_by(q)


def sort_team(sorting_string, query):
    if not sorting_string:
        return query
    sorting_string = sorting_string[0].strip()
    if "@" in sorting_string:
        return sort_team_by(sorting_string.replace("@", ""), query, True)
    else:
        return sort_team_by(sorting_string.replace('@', ""), query, False)

def filter_teams(query, all):
    if not query:
        return all
    filters = query[0].strip().split(',')
    
    # league filters
    if 'leagueNBA' in filters:
        all = all.filter(Team.league == 'NBA')
    elif 'leagueWNBA' in filters:
        all = all.filter(Team.league == 'WNBA')
    
    # conference filters
    if 'conferenceNBAEast' in filters:
        all = all.filter(Team.conference == 'Eastern')
    elif 'conferenceNBAWest' in filters:
        all = all.filter(Team.conference == 'Western')
    elif 'conferenceWNBAEast' in filters:
        all = all.filter(Team.conference == 'EAST')
    elif 'conferenceWNBAWest' in filters:
        all = all.filter(Team.conference == 'WEST')
    
    # state filters
    r = re.compile('state.*')
    filters = list(filter(r.match, filters))
    print(filters)
    if len(filters) > 0:
        state = filters[0][6:len(filters[0])]
        all = all.filter(Team.state == state)
    return all

    # TODO: implement more cases (dynamic filters)
    # TODO: need to handle invalid filter case?


def search_teams(searching_string, query):
    if not searching_string:
        return query
    searching_string = searching_string[0].strip()
    return new_query(searching_string.split(), query)


def new_query(search_terms, query):
    searches = []
    for term in search_terms:
        for attr in searchable_attributes:
            searches.append(getattr(Team, attr).ilike(f"%{term}%"))
    query = query.filter(or_(*searches))
    return query

