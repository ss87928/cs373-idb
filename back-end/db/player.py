# code citation: https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/blob/main/backend/db/job.py


from .models import Player, Coach, Team, db as database
from .schemas import player_schema
from sqlalchemy import or_
import re
from .helpers import get_query
from .helpers import object_as_dict
from .team import team_handler


team_names = [
    "Atlanta Hawks",
    "Brooklyn Nets",
    "Boston Celtics",
    "Charlotte Hornets",
    "Chicago Bulls",
    "Cleveland Cavaliers",
    "Dallas Mavericks",
    "Denver Nuggets",
    "Detroit Pistons",
    "Golden State Warriors",
    "Houston Rockets",
    "Indiana Pacers",
    "LA Clippers",
    "Los Angeles Lakers",
    "Memphis Grizzlies",
    "Miami Heat",
    "Milwaukee Bucks",
    "Minnesota Timberwolves",
    "New Orleans Pelicans",
    "New York Knicks",
    "Oklahoma City Thunder",
    "Orlando Magic",
    "Philadelphia 76ers",
    "Phoenix Suns",
    "Portland Trail Blazers",
    "Sacramento Kings",
    "San Antonio Spurs",
    "Toronto Raptors",
    "Utah Jazz",
    "Washington Wizards",
    "Atlanta Dream",
    "Chicago Sky",
    "Connecticut Sun",
    "Dallas Wings",
    "Indiana Fever",
    "Los Angeles Sparks",
    "Las Vegas Aces",
    "Minnesota Lynx",
    "New York Liberty",
    "Phoenix Mercury",
    "Seattle Storm",
    "Washington Mystics",
]

searchable_attributes = ["name", "league", "pos", "teamid"]


def player_handler(player_id):
    player = Player.query.get(player_id)
    d = object_as_dict(player)
    coach = (
        database.session.query(
            Coach.__table__.columns["name"], Coach.__table__.columns["coachid"]
        )
        .filter(Coach.teamid == d["teamid"])
        .filter(Coach.pos == "Head Coach")
        .all()
    )

    coach_data = {}
    coach_data["coachid"] = coach[0][1]
    coach_data["name"] = coach[0][0]
    d["coach"] = coach_data

    team = (
        database.session.query(
            Team.__table__.columns["name"], Team.__table__.columns["id"]
        )
        .filter(Team.id == d["teamid"])
        .all()
    )
    team_data = {}
    team_data["id"] = team[0][1]
    team_data["name"] = team[0][0]
    d["team"] = team_data

    return d


def sort_player_by(sorting_string, query, desc):
    q = None
    try:
        q = getattr(Player, sorting_string)
    except:
        return query

    if desc:
        return query.order_by(q.desc())
    else:
        return query.order_by(q)


def sort_player(sorting_string, query):
    if not sorting_string:
        return query
    sorting_string = sorting_string[0].strip()
    if "@" in sorting_string:
        return sort_player_by(sorting_string.replace("@", ""), query, True)
    else:
        return sort_player_by(sorting_string.replace('@', ""), query, False)

def filter_players(query, all):
    if not query:
        return all
    filters = query[0].strip().split(',')
    
    # league filters
    if 'leagueNBA' in filters:
        all = all.filter(Player.league == 'NBA')
    elif 'leagueWNBA' in filters:
        all = all.filter(Player.league == 'WNBA')
    
    # gender filters
    if 'genderM' in filters:
        all = all.filter(Player.gender == 'M')
    elif 'genderF' in filters:
        all = all.filter(Player.gender == 'F')
    
    # position filters
    if 'posPG' in filters:
        all = all.filter(Player.pos == 'PG')
    elif 'posSG' in filters:
        all = all.filter(Player.pos == 'SG')
    elif 'posSF' in filters:
        all = all.filter(Player.pos == 'SF')
    elif 'posPF' in filters:
        all = all.filter(Player.pos == 'PF')
    elif 'posC' in filters:
        all = all.filter(Player.pos == 'C')
    return all
    
    # TODO: implement more cases (dynamic filters)
    # TODO: need to handle invalid filter case?


def search_players(searching_string, query):
    if not searching_string:
        return query
    searching_string = searching_string[0].strip()
    return new_query(searching_string.split(), query)


def new_query(search_terms, query):
    searches = []
    for term in search_terms:
        for attr in searchable_attributes:
            if attr == "teamid":
                for index, name in enumerate(team_names):
                    if re.search(f".*{term.lower()}.*", name.lower()):
                        searches.append(getattr(Player, attr) == str(index))
            searches.append(getattr(Player, attr).ilike(f"%{term}%"))
    query = query.filter(or_(*searches))
    return query