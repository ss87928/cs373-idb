# code citation: https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/blob/main/backend/db/job.py


import sys
import re

sys.path.append("..")
from .models import Coach, Player, Team, db as database
from .schemas import coach_schema
from sqlalchemy import or_, desc
from .helpers import get_query, object_as_dict

# import app

team_names = [
    "Atlanta Hawks",
    "Brooklyn Nets",
    "Boston Celtics",
    "Charlotte Hornets",
    "Chicago Bulls",
    "Cleveland Cavaliers",
    "Dallas Mavericks",
    "Denver Nuggets",
    "Detroit Pistons",
    "Golden State Warriors",
    "Houston Rockets",
    "Indiana Pacers",
    "LA Clippers",
    "Los Angeles Lakers",
    "Memphis Grizzlies",
    "Miami Heat",
    "Milwaukee Bucks",
    "Minnesota Timberwolves",
    "New Orleans Pelicans",
    "New York Knicks",
    "Oklahoma City Thunder",
    "Orlando Magic",
    "Philadelphia 76ers",
    "Phoenix Suns",
    "Portland Trail Blazers",
    "Sacramento Kings",
    "San Antonio Spurs",
    "Toronto Raptors",
    "Utah Jazz",
    "Washington Wizards",
    "Atlanta Dream",
    "Chicago Sky",
    "Connecticut Sun",
    "Dallas Wings",
    "Indiana Fever",
    "Los Angeles Sparks",
    "Las Vegas Aces",
    "Minnesota Lynx",
    "New York Liberty",
    "Phoenix Mercury",
    "Seattle Storm",
    "Washington Mystics",
]

searchable_attributes = ["name", "league", "pos", "teamid"]


def coach_handler(coach_id):
    print(coach_id)
    coach = Coach.query.get(coach_id)
    d = object_as_dict(coach)

    player = (
        database.session.query(
            Player.__table__.columns["name"], Player.__table__.columns["id"]
        )
        .filter(Player.teamid == d["teamid"])
        .order_by(desc(Player.__table__.columns["ppg"]))
        .limit(1)
        .all()
    )
    player_data = {}
    player_data["id"] = player[0][1]
    player_data["name"] = player[0][0]
    d["featuredplayer"] = player_data
    # d["featuredplayer"] = player[0][1]

    team = (
        database.session.query(
            Team.__table__.columns["name"], Team.__table__.columns["id"]
        )
        .filter(Team.id == d["teamid"])
        .all()
    )
    team_data = {}
    team_data["id"] = team[0][1]
    team_data["name"] = team[0][0]
    d["team"] = team_data
    return d


def sort_coach_by(sorting_string, query, desc):
    q = None
    try:
        q = getattr(Coach, sorting_string)
    except:
        return query

    if desc:
        return query.order_by(q.desc())
    else:
        return query.order_by(q)


def sort_coaches(sorting_string, query):
    if not sorting_string:
        return query
    sorting_string = sorting_string[0].strip()
    if "@" in sorting_string:
        return sort_coach_by(sorting_string.replace("@", ""), query, True)
    

def sort_coaches(sorting_string, query):
    if not sorting_string:
        return query
    sorting_string = sorting_string[0].strip()
    if "@" in sorting_string:
        return sort_coach_by(sorting_string.replace("@", ""), query, True)
    else:
        return sort_coach_by(sorting_string.replace('@', ""), query, False)

def filter_coaches(query, all):
    if not query:
        return all
    # split all filters
    filters = query[0].strip().split(',')
    
    # league filters
    if 'leagueNBA' in filters:
        all = all.filter(Coach.league == 'NBA')
    elif 'leagueWNBA' in filters:
        all = all.filter(Coach.league == 'WNBA')

        # position filters
    if 'posHC' in filters:
        all = all.filter(Coach.pos == 'Head Coach')
    elif 'posAC' in filters:
        all = all.filter(Coach.pos == 'Assistant Coach')
    return all

    # TODO: implement more cases (dynamic filters)
    # TODO: need to handle invalid filter case?



def search_coaches(searching_string, query):
    if not searching_string:
        return query
    searching_string = searching_string[0].strip()
    return new_query(searching_string.split(), query)

    



def new_query(search_terms, query):
    searches = []
    for term in search_terms:
        for attr in searchable_attributes:
            if attr == "teamid":
                for index, name in enumerate(team_names):
                    if re.search(f".*{term.lower()}.*", name.lower()):
                        searches.append(getattr(Coach, attr) == str(index))
            searches.append(getattr(Coach, attr).ilike(f"%{term}%"))
    query = query.filter(or_(*searches))
    return query
