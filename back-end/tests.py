import unittest
import requests

import db.models
import db.schemas
from db.models import app

API_HOST = "https://api.low-ball.me"


class FlaskTest(unittest.TestCase):
    def test_player_model(self):
        player = db.models.Player.query.get("0")
        d = player.__dict__
        self.assertEqual(d["name"], "Precious Achiuwa")

    def test_coach_model(self):
        coach = db.models.Coach.query.get("0")
        d = coach.__dict__
        self.assertEqual(d["name"], "Nate McMillan")

    def test_team_model(self):
        team = db.models.Team.query.get("0")
        d = team.__dict__
        self.assertEqual(d["name"], "Atlanta Hawks")

    def test_players_content_keys(self):
        response = requests.get(API_HOST + "/players")
        response = response.json()
        self.assertEqual(
            set(response["data"][0]),
            {
                "id",
                "name",
                "league",
                "gp",
                "ppg",
                "apg",
                "rpg",
                "piclink",
                "gender",
                "pos",
                "teamid",
                "salary",
                "coachid",
                "youtubelink",
                "salary_int"
            },
        )

    def test_coaches_content_keys(self):
        response = requests.get(API_HOST + "/coaches")
        response = response.json()
        self.assertEqual(
            set(response["data"][0]),
            {
                "coachid",
                "name",
                "league",
                "careergames",
                "careerwins",
                "careerlosses",
                "careerratio",
                "seasonswithfranchise",
                "seasonsoverall",
                "seasongames",
                "seasonwins",
                "seasonlosses",
                "seasonratio",
                "piclink",
                "teamid",
                "pos",
                "youtubelink"
            },
        )

    def test_teams_content_keys(self):
        response = requests.get(API_HOST + "/teams")
        response = response.json()
        self.assertEqual(
            set(response["data"][0]),
            {
                "id",
                "name",
                "league",
                "gp",
                "wins",
                "losses",
                "ratio",
                "piclink",
                "rank",
                "abbreviation",
                "conference",
                "championships",
                "state",
                "youtubelink",
                "coachid"
            },
        )

    def test_player_content_keys(self):
        response = requests.get(API_HOST + "/player/0")
        response = response.json()
        self.assertEqual(
            set(response),
            {
                "id",
                "name",
                "league",
                "gp",
                "ppg",
                "apg",
                "rpg",
                "piclink",
                "gender",
                "pos",
                "teamid",
                "team",
                "salary",
                "coach",
                "youtubelink",
                "salary_int"
            },
        )

    def test_coach_content_keys(self):
        response = requests.get(API_HOST + "/coach/0")
        response = response.json()
        self.assertEqual(
            set(response),
            {
                "coachid",
                "name",
                "league",
                "careergames",
                "careerwins",
                "careerlosses",
                "careerratio",
                "seasonswithfranchise",
                "seasonsoverall",
                "seasongames",
                "seasonwins",
                "seasonlosses",
                "seasonratio",
                "piclink",
                "teamid",
                "team",
                "featuredplayer",
                "pos",
                "youtubelink"
            },
        )

    def test_team_content_keys(self):
        response = requests.get(API_HOST + "/team/0")
        response = response.json()
        self.assertEqual(
            set(response),
            {
                "id",
                "name",
                "league",
                "gp",
                "wins",
                "losses",
                "ratio",
                "piclink",
                "rank",
                "abbreviation",
                "conference",
                "championships",
                "state",
                "players",
                "coach",
                "youtubelink"
            },
        )
    
    def test_players_query(self):
        response = requests.get(
            API_HOST + "/players?search=lakers"
        )
        self.assertEqual(response.status_code, 200)

    def test_coaches_query(self):
        response = requests.get(
            API_HOST + "/coaches?search=lakers"
        )
        self.assertEqual(response.status_code, 200)

    def test_teams_query(self):
        response = requests.get(
            API_HOST + "/teams?search=lakers"
        )
        self.assertEqual(response.status_code, 200)



if __name__ == "__main__":
    with app.app_context():
        unittest.main()
