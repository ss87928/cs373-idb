# code citation: team Get That Bread at https://gitlab.com/Nathaniel-Nemenzo/getthatbread

from flask import Flask, jsonify, request

from db.models import app, db as database, Player, Coach, Team
import db.player, db.coach, db.team
from db.schemas import player_schema, coach_schema, team_schema
from flask_cors import CORS

from db.helpers import object_as_dict, get_query, player_to_dict

from sqlalchemy import desc

LIMIT = 10

CORS(app)


def get_offset(page):
    return (page - 1) * 10


@app.route("/")
def hello_world():
    return "<p> Welcome to Lowball API! </p>"


@app.route("/coaches", methods=["GET"])
def get_coaches():
    queries = request.args.to_dict(flat=False)

    coaches = database.session.query(*Coach.__table__.columns)
    
    search = get_query('search', queries)
    if search:
        coaches = db.coach.search_coaches(search, coaches)

    sort = get_query('sort', queries)
    if sort:
        coaches = db.coach.sort_coaches(sort, coaches)
        
    filter = get_query('filter', queries)
    if filter:
        coaches = db.coach.filter_coaches(filter, coaches)
    
    all_coaches_count = len(coach_schema.dump(coaches))

    if "page" in queries:
        page = int(queries["page"][0])
        coaches = coaches.offset(get_offset(page)).limit(LIMIT)

    result = coach_schema.dump(coaches)
    return {"data": result, "meta_count": all_coaches_count, "count": len(result)}


@app.route("/players", methods=["GET"])
def get_players():
    queries = request.args.to_dict(flat=False)
    

    players = (
            database.session.query(
                *Player.__table__.columns, Coach.__table__.columns["coachid"]
            )
            .filter(Player.teamid == Coach.teamid)
            .filter(Coach.pos == "Head Coach")
        )
    
    search = get_query('search', queries)
    if search:
        players = db.player.search_players(search, players)

    sort = get_query('sort', queries)
    if sort:
        players = db.player.sort_player(sort, players)
    
    filter = get_query('filter', queries)
    if filter:
        players = db.player.filter_players(filter, players)

    all_players_count = len(player_schema.dump(players))

    if "page" in queries:
        page = int(queries["page"][0])
        players = (
            players
            .offset(get_offset(page))
            .limit(LIMIT)
        )


    result = player_schema.dump(players)
    return {"data": result, "meta_count": all_players_count, "count": len(result)}


@app.route("/teams", methods=["GET"])
def get_teams():
    queries = request.args.to_dict(flat=False)
    

    teams = (
            database.session.query(
                *Team.__table__.columns, Coach.__table__.columns["coachid"]
            )
            .filter(Team.id == Coach.teamid)
            .filter(Coach.pos == "Head Coach")
        )
    
    search = get_query('search', queries)
    if search:
        teams = db.team.search_teams(search, teams)
    
    sort = get_query('sort', queries)
    if sort:
        teams = db.team.sort_team(sort, teams)
    
    filter = get_query('filter', queries)
    if filter:
        teams = db.team.filter_teams(filter, teams)

    # print(type(teams))
    all_teams_count = len(team_schema.dump(teams))

    if "page" in queries:
        page = int(queries["page"][0])
        teams = (
            teams
            .offset(get_offset(page))
            .limit(LIMIT)
        )
        

    result = team_schema.dump(teams)
    return {"data": result, "meta_count": all_teams_count, "count": len(result)}


@app.route("/coach/<coach_id>", methods=["GET"])
def get_coach(coach_id=0):
    d = db.coach.coach_handler(coach_id)
    return jsonify(d)


@app.route("/player/<player_id>", methods=["GET"])
def get_player(player_id=0):
    d = db.player.player_handler(player_id)
    return jsonify(d)


@app.route("/team/<team_id>", methods=["GET"])
def get_team(team_id=0):
    d = db.team.team_handler(team_id)
    return jsonify(d)

@app.route("/search", methods=["GET"])
def search():
  """
  I 
  """
  pass


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
