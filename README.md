**Canvas / Discord group number:** 10-4

**Group members**
Member          | UT EID 
--------------- | -------
Srivatsan Srikanth   | ss87928
Avinash Gupta | arg4825
Tyler Roberts  | tkr464
Evelyn Vo     | ev8744
Sizhan Xu       | sx2546
 
**Git SHA**

Phase | SHA
------|--------
Phase I | 26680fd75cbcb7d21cb7094ef743947c81ec0383
Phase II | 442aa2c46e77d199208ba0ad509b4cbffb4d6d26
Phase III | 96af9e7aa47944c64ef4595a7ad716fd6609030c
Phase IV | b1cb314c02938f658f31c2bdaeb281f6753c4d02

**Project leader**:
Phase | Leader
------|---------
Phase I | Srivatsan Srikanth
Phase II | Evelyn Vo
Phase III | Sizhan Xu 
Phase IV | Tyler Roberts / Avinash Gupta

**URL of the GitLab repo:** https://gitlab.com/lowball/cs373-idb

**Website Link**: https://www.low-ball.me/

**GitLab pipelines**: https://gitlab.com/lowball/cs373-idb/-/pipelines


**Completion Times**:

Phase I:
Member          | Estimated | Actual
--------------- | ------- | --------
Srivatsan Srikanth   | 6 hours | 7 hours
Avinash Gupta | 5 hours | 6 hours
Tyler Roberts  | 10 hours | 7 hours
Evelyn Vo     | 5 hours | 8 hours
Sizhan Xu       | 6 hours | 7 hours

Phase II:
Member          | Estimated | Actual
--------------- | ------- | --------
Srivatsan Srikanth   | 10 hours | 12 hours
Avinash Gupta | 8 hours | 10 hours
Tyler Roberts  | 8 hours | 12 hours
Evelyn Vo     | 7 hours | 7 hours
Sizhan Xu       | 10 hours | 15 hours

Phase III:
Member          | Estimated | Actual
--------------- | ------- | --------
Srivatsan Srikanth   | 8 hours | 8 hours
Avinash Gupta | 6 hours | 5 hours
Tyler Roberts  | 6 hours | 6 hours
Evelyn Vo     | 8 hours | 8 hours
Sizhan Xu       | 9 hours | 9 hours

Phase IV:
Member          | Estimated | Actual
--------------- | ------- | --------
Srivatsan Srikanth   | 9 hours | 7 hours
Avinash Gupta | 7 hours | 7 hours
Tyler Roberts  | 8 hours | 7 hours
Evelyn Vo     | 6 hours | 8 hours
Sizhan Xu       | 9 hours | 10 hours


**Additional Details**

**Names of the team members:** Srivatsan Srikanth, Avinash Gupta, Tyler Roberts, Evelyn Vo, and Sizhan Xu

**Name of the project (alphanumeric, no spaces, max 32 chars, this will also be your URL):** Lowball.me

**URL of the GitLab repo:** https://gitlab.com/ss87928/cs373-idb

**The proposed project:** A basketball reference webpage that displays statistics for a multitude of players, teams, and team coaches across different leagues, with an emphasis on disparities between the salaries of players and team coaches in the NBA, the NBA G-League, and the WNBA.

**URLs of at least three disparate data sources that you will programmatically scrape using a RESTful API (be very sure about this):** 
- https://www.basketball-reference.com/
- https://www.nba.com/
- https://www.balldontlie.io/#get-all-players
- https://developer.sportradar.com/docs/read/basketball/NBA_G_League_v7

**At least three models:** Players, Coaches, Teams

**An estimate of the number of instances of each model:** Players - 150+, Coaches - 100+, Teams - 50+

**Describe at least five of those attributes for each model that you could filter by or sort by on the model (grid) pages:**

- Players
    - League Type (G-League/Main/WNBA)
    - Games Played (GP)
    - Points Per Game (PPG)
    - Assists Per Game (APG)
    - Rebounds Per Game (RPG)

- Coaches
    - League Type (G-League/Main/WNBA)
    - Games Coached
    - Games Won
    - Games Lost
    - Win Percentage

- Teams
    - League Type (G-League /Main/WNBA)
    - Games Won
    - Games Lost
    - Win Percentage
    - Total Championships Won


**Describe at least five additional attributes for each model that you could search for on the instance pages:**

- Players
    - Name
    - Position
    - Salary
    - Years Played
    - Place of Origin
    - Gender

- Coaches
    - Names
    - Salary
    - Years Coached
    - Record (Playoff/Regular Season)
    - Age

- Teams
    - Names
    - Conference
    - Years Active
    - Home State
    - Rank


**Describe at least two types of media for each model that you could display on the instance pages:**

- Players
    - Twitter/Instagram Feed
    - Images of Gameplay

- Coaches
    - Twitter/Instagram Feed
    - Recent News Feed

- Teams
    - Twitter/Instagram Feed
    - Videos of Gameplay


**What three questions will you answer due to doing this data synthesis on your site?**
- What is the pay gap among players of different genders?
- What is the pay gap among players in different leagues?
- What is the pay gap among coaches with the same level of experience?
